//Author: Matthew Hoppesch
//Note: This must be run with gain curve data in the same directory
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TLegend.h"

using namespace std;


//gets your TGraph from the saved canvas
TGraphErrors* getFit(string tubename){
  vector <double> fit;
  TFile* f = new TFile(Form("%s_EstGain.root",(tubename).c_str()),"read");
  TCanvas * c1 = (TCanvas*)f->Get("Gain");
  TGraphErrors*  g1 = (TGraphErrors*)c1->GetListOfPrimitives()->FindObject("Graph");
  TF1* f1 = (TF1*)g1->GetListOfFunctions()->At(0);
  return g1;
}

//sets style of tGraph
void equipTGraph( TGraphErrors* g1, int _style, int _color ){
  g1->SetMarkerStyle( _style );
  g1->SetMarkerColor( _color );
  g1->SetLineColor( _color );
  g1->SetMarkerSize( 0.9 );
}

//Draws TMultiGraph  change most formatting besides legend in here
void DrawMTGraph(TMultiGraph*  m1, TCanvas * canv1,TLegend* l, string title){
  canv1->cd(0);

  m1->GetXaxis()->SetTitle("SUPPLY VOLTAGE (V)");
  m1->GetYaxis()->SetTitle("GAIN");
  m1->GetYaxis()->SetMaxDigits(3);
  m1->GetYaxis()->SetTitleSize(0.04);
	m1->GetXaxis()->SetTitleSize(0.04);
	m1->GetXaxis()->SetLabelSize(0.03);
	m1->GetYaxis()->SetLabelSize(0.03);
  // Set Limits
  m1->GetYaxis()->SetRangeUser(5e+5,13e+6);

	gStyle->SetTitleFontSize(0.07);
	gStyle->SetOptStat(0);

	gPad->SetLeftMargin(0.1);
	gPad->SetRightMargin(0.05);
	gPad->SetTopMargin(0.05);
	gPad->SetBottomMargin(0.1);
	TLatex* lx = new TLatex();
	m1 ->Draw("AP");

	lx->SetTextAlign(11);
	lx->SetTextFont( 62 );
	lx->SetTextSize( 0.05 );
	lx->DrawLatexNDC(0.28,0.90,Form("PF-RPD Prototype: Side %s", title.c_str()));
	lx->SetTextFont( 42 );
	lx->SetTextSize( 0.036 );
	lx->DrawLatexNDC(0.28,0.86,"Gain Calibration");
	lx->SetTextFont( 42 );
	lx->DrawLatexNDC(0.28,0.82, "TestBeam 2022" );

  l->Draw();
  canv1->Print(Form("RPD_GainPlot_%s.pdf", title.c_str()));
}

void macroGainPlot(){

  vector<TMultiGraph *> mg;
  vector<TLegend*> leg;
  leg.push_back(new TLegend(0.15,0.33,0.23,0.95));
  leg.push_back(new TLegend(0.15,0.33,0.23,0.95));
  leg.push_back(new TLegend(0.15,0.33,0.23,0.95));
  //mapping provided by Daniel
  vector<vector<string>> rpd_map = {{"WB1936","WB3878","WA9749","RX4298","RX4484","WA2810","WB2014","WB1929","WA2812","WB3829","WA2777","WA9762","RX3296","WA9939","RX4685","RX4694"},{"RX4886","RX5702","WA1905","8A318","RX4904","RX4750","RX4527","RX3090","RX3355","RX3957","8A323","RX2349","8A334","RX4694","RX4446","RX4907"},{"WA9749","RX4428","RX4101","WA2777","RX4298","RX5690","RX3648","RX5058","WA2810","RX3406","RX5199","RX4866","WA9939","WB1929","WB1936","RX3477"}};
  vector<vector<TGraphErrors*>> vGraph;
  vGraph.resize(3);
  int num_tubes = 16;


//get Tgraphs for correct tubes
  for(int i = 0; i<3; i++){
    mg.push_back(new TMultiGraph());
    for(int j = 0;j<num_tubes; j++){
      vGraph.at(i).push_back(getFit(rpd_map.at(i).at(j)));
    }
  }

  //set styles and add to TMultiGraph
  for(int i = 0; i<3; i++){
    for(int j = 0;j<num_tubes; j++){

        if(j+1==3){
            equipTGraph( vGraph.at(i).at(j), j+20, kGreen+4); ((TF1*)vGraph.at(i).at(j)->GetListOfFunctions()->At(0))->SetLineColor(kGreen+3);}
        else if(j+1==5){
            equipTGraph( vGraph.at(i).at(j), j+20, kYellow+3); ((TF1*)vGraph.at(i).at(j)->GetListOfFunctions()->At(0))->SetLineColor(kYellow+3);}
        else if(j+1>9){
          equipTGraph( vGraph.at(i).at(j), j+20, j+31); ((TF1*)vGraph.at(i).at(j)->GetListOfFunctions()->At(0))->SetLineColor(j+31);}

        else{
            equipTGraph( vGraph.at(i).at(j), j+20, j+1); ((TF1*)vGraph.at(i).at(j)->GetListOfFunctions()->At(0))->SetLineColor(j+1);}

      mg.at(i) -> Add(vGraph.at(i).at(j));
      ((TF1*)vGraph.at(i).at(j)->GetListOfFunctions()->At(0))->SetLineStyle(kDashed);
      leg.at(i)->AddEntry( vGraph.at(i).at(j), Form("Channel %d",j + 1), "pl");

  }}

  //Print TMultiGraph and save to pdf
  TCanvas *c1 = new TCanvas("c1","PFRPD Gain Curves",200,10,700,500);
  DrawMTGraph(mg.at(1),c1,leg.at(1),"A");
  TCanvas *c2 = new TCanvas("c2","PFRPD Gain Curves",200,10,700,500);
  DrawMTGraph(mg.at(2),c2,leg.at(2),"C");
  TCanvas *c3 = new TCanvas("c3","PFRPD Gain Curves",200,10,700,500);
  DrawMTGraph(mg.at(0),c3,leg.at(0),"TB2021");
}
