R__LOAD_LIBRARY(libMyTest.so);

using namespace std;

TFile *f;
TCanvas *c;
TH1F *h1, *h2, *hWF, *test;
TF1 *chargeFit,*peakFit;
TVirtualPad *p1, *p2, *p3;
TTimer *timer;
//TLine *hWFline;

void updateCanvas(){
  p1->Modified();
  p1->Update();
  p2->Modified();
  p2->Update();
  p3->Modified();
  p3->Update();
}

void start(){
  gSystem->Load("libMyTest.so");

  rcdaqopen();
  pstart();

  c = new TCanvas("data","data",900,300);
  c->Divide(3,1);

  p1 = c->cd(1);
  hWF = (TH1F*)gDirectory->Get("hWF");
  hWF->Draw();

  test = (TH1F*)gDirectory->Get("test");
  test->SetLineColor(kRed);
  test->Draw("same");

  p2 = c->cd(2);
  p2->SetLogy();
  h1 = (TH1F*)gDirectory->Get("charge");
  h1->Draw();

  //h1fit->Draw("same");

  p3 = c->cd(3);
  p3->SetLogy();
  h2 = (TH1F*)gDirectory->Get("peak");
  h2->Draw();

  timer = new TTimer("updateCanvas()", 3000);
  timer->TurnOn();


void stop(string pmtSN = "null", int voltage = 0){
  timer->TurnOff();
  delete timer;
  //Delete timer to stop memory leak???

  //chargeFit = new TF1("chargeFit","gaus",1,1.5);
  //TF1 chargeFit("chargeFit","gaus",1,1.5);
  //hWFline = (TLine*)gDir
  //h1->Fit(chargeFit,"RE");

  //peakFit = new TF1("peakFit","gaus",10,30);
  //TF1 peakFit("peakFit","gaus",0.1,2);
  //h2->Fit(peakFit, "RE");


  //hWF->Draw();
  //h1->Draw();
  //h2->Draw();
//  updateCanvas();

  //h2->Fit(&gaussFit, "E");


  f = new TFile( Form("%s_%d.root",pmtSN.c_str(), voltage), "recreate");

  h1->Write();
  h2->Write();

  f->Close();

  delete f;

}
void cloose() {
  delete c;
}
}
