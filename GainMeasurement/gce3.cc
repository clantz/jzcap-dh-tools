#include <vector>
#include <iostream>
#include "TPaveStats.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TPad.h"
#include "TGraphErrors.h"
#include "TSpectrum.h"

int gce3(){
  gStyle->SetOptStat(0);
  //select PMT
  string pmtName = "WA2810";
  cout << ("PMT Serial: ");
  cin >> pmtName;
  float mult = 2;
  cout << ("gauss range?: ");
  cin >> mult;

  TCanvas *c = new TCanvas("data","data",900,700);
  c->Divide(1,1);//Sets TCanvas as a 1x1
  c->SetLogy();//Sets y-axis on log scale

  TCanvas *c1 = new TCanvas("GainCrv","dataOvl",900,700);
  gStyle->SetOptStat(0);

  Double_t par[9];//Define a parameter arrow for total function
  //float ChiSquareTot = 0;

  //vectors for different voltages DESCENDING ORDER
  std::vector < int > voltage;
  voltage.push_back(1450);voltage.push_back(1400);voltage.push_back(1300);voltage.push_back(1200);voltage.push_back(1100);//voltage.push_back(1000);
  std::vector < int > colors;
  colors.push_back(kRed+2); colors.push_back(kOrange+2); colors.push_back(kYellow+2); colors.push_back(kGreen+2); colors.push_back(kBlue); colors.push_back(kViolet);

  std::vector < TH1F* > hist;

  // std::vector < double > rmin;
  //rmin.push_back(0.3);rmin.push_back(0.3);rmin.push_back(0.25);rmin.push_back(0.2);rmin.push_back(0.05);//rmin.push_back(0.05);

  std::vector < double > erange;
  erange.push_back(0.2);erange.push_back(0.16);erange.push_back(0.12);erange.push_back(0.08);erange.push_back(0.04);
  
  Double_t y[5];
  Double_t x[5] = {1450,1400,1300,1200,1100};
  Double_t yerr[5];
  Double_t vpeaks[5];
  //Loop on different voltages
  for( int iF = 0; iF < (int)voltage.size(); iF++){
    TFile *f = new TFile( Form("%s/%s_%d.root",pmtName.c_str(),pmtName.c_str(), voltage.at(iF)),"read");
    TH1F *charge = (TH1F*)f->Get("charge");
    c->cd();
    c->Clear();
    
    //peak finder - very effective, cuts out points below 0.1
    TSpectrum *s = new TSpectrum(2);
    //!!!!!!!!!!!!!adjust the second argument of below line to improve peak search (in range of 4-12, higher corresponds to steeper peaks)!!!!!!
    Int_t nfound = s->Search(charge,5,"",1e-03);
    Double_t *xpeaks;
    Double_t *ypeaks;
    float goodpeakamp = 0;
    float goodpeak = 0;
    xpeaks = s->GetPositionX();
    ypeaks = s->GetPositionY();
    for(int ipk = 0; ipk < 3; ipk++) {
      if(xpeaks[ipk] > 0.1) {
	goodpeak = xpeaks[ipk];
	goodpeakamp = ypeaks[ipk];
      }
    }

    float fitrange = mult * erange.at(iF);
    
    TF1*estimate = new TF1("estimate","gaus", goodpeak-fitrange, goodpeak+fitrange );
    //estimate->SetParName(0,"amp_gauss"); 
    //estimate->SetParName(1,"mean_gauss");
    //estimate->SetParName(2,"dev_gauss");

    estimate->SetParameter(0, goodpeakamp);
    estimate->SetParameter(0, goodpeak);
    estimate->SetParameter(0, fitrange);

    estimate->SetLineColor(kBlack);
    estimate->SetLineWidth(2);
    
    charge->SetTitle( Form("%s",pmtName.c_str()) );
    charge->SetLineColor(colors.at(iF));
    charge->Draw();

    charge->Fit("estimate","RE");
    
    float center = estimate->GetParameter(1);
    float stdev = estimate->GetParameter(2);
    cout << "----";cout << center;cout << stdev;cout << "----";
    
    //collect means
    double mean = estimate->GetParameter(1);
    //gain.push_back(mean/(1.60218*10^-7));
    y[iF] = mean/(1.60218e-7);
    double err = estimate->GetParError(1);
    //gainerrors.push_back(err/1.60218*10^-7);
    yerr[iF] = err/(1.60218e-7);
    vpeaks[iF] = goodpeak/1.60218e-7;

    //Let's get the bin width a la Ricky
    double xRange = charge->GetXaxis()->GetXmax()-charge->GetXaxis()->GetXmin();
    double nBins = charge->GetXaxis()->GetNbins();
    double binWidth = xRange / nBins;
    charge->GetYaxis()->SetTitle( Form("Counts/[%.3f pC]", binWidth));

    hist.push_back(charge);
  }
  //gStyle->SetOptStat("e");
  // gStyle->SetOptFit(1001);
  for(int i = 0; i < (int)hist.size(); i++){
    if( i > 0 ) hist.at(i)->Draw("SAME");
    else {
      hist.at(i)->Draw();
    }
  }

  //gain curve stuff
  c1->cd();
  TGraphErrors *gcurve = new TGraphErrors(5,x,y,0,yerr);
  gcurve->SetTitle(Form("%s Estimate Gain Curve",pmtName.c_str()));
  gcurve->SetMarkerColor(4);
  gcurve->SetMarkerStyle(21);
  gcurve->Draw("ap");

  TF1*gcurvefit = new TF1("gcurvefit","[0]*x^(8*[1])", 1100,1450);
  gcurvefit->SetParLimits(1,0.6,0.7);
  gStyle->SetOptStat(0);
  gcurve->Fit(gcurvefit, "RE");
  gcurvefit->Draw("SAME");
  
  gStyle->SetOptStat(0);

  TFile* output = new TFile( Form("%s_EstGain.root", pmtName.c_str()), "RECREATE" );
  output->cd();
  c1->Write("Gain");
  output->Close();

  return 0;
}
