
#include <iostream>
#include <pmonitor.h>
#include "MyTest.h"

#include <cmath>
#include <TCanvas.h>
#include <TH1.h>
#include <TF1.h>
#include <TH2.h>
#include <TFile.h>
#include <fstream>
//#include <TLine.h>

int init_done = 0;

using namespace std;

TCanvas *c;
TH1F *h1;
TH1F *h2;
TH1F *hWF;
TH1F *test;
//TLine *hWFline;
//TF1 *chargeFit;
//TF1 *peakFit;

TFile *f;
bool firstEvent = true;

int pinit()
{

  if (init_done) return 1;
  init_done = 1;

  h1 = new TH1F ( "charge","Charge; Charge [pC]", 500, 0,6); //bins were 100
  h2 = new TH1F ( "peak","Peak; Amplitude [mV]", 150, 0, 150);
  hWF = new TH1F ( "hWF","Raw Waveform; Time [bin = 0.2ns];Amplitude [mV]", 1024, 0, 1023);
  test = new TH1F ( "test","Raw Waveform; Time [bin = 0.2ns];Amplitude [mV]", 1024, 0, 1023);
  //test->FillRandom("gaus",1000);
  //chargeFit = new TF1 ("chargeFit","gaus", 0.2,0.8);
  //peakFit= new TF1 ("peakFit","gaus",10,30 );
  return 0;

}


/*
int process_event (Event * e)
{
  hWF->Reset();
  int start = 210;
  int stop = 250;
  float val;
  float charge = 0;
  float ped = 0;
  float max = 0;

  Packet *p = e->getPacket(1001);

  if (p) {

      for(int bin = 0; bin < 1024; bin++){
	val = p->rValue(bin, 0);
	hWF->SetBinContent(bin, val );
	if(bin < start){
	  ped += val/start;
	}else if (bin < stop){
	  if(val < max) max = val;
	  //0.2ns per bin * value in mV divided by input impedance (50 ohm)
	  charge += .2*(val-ped)/50;
	}
      }
      h1->Fill(-charge);
      h2->Fill(-(max-ped));

      delete p;

    }
  return 0;
}
*/


int process_event (Event * e){
  float thresh = -4;
  int start = 100;
  float charge = 0;
  float val;
  float val2;
  float ped = 0;
  float max = 0;
  float pedmax = 0;
  bool hit = false;
  float pedint = 0;
  float bincounter = 0;
  float spikecounter = 0;
  float spikewidth = 2;
  float oldbin = 0;

  Packet *p = e->getPacket(1001);
  if (p) { //if the packet exists, process data
    for(int bin = 10; bin < 1000 ; bin++){ //Loop through each bin   1024 bin end
      val = p->rValue(bin, 0); //get bin value
      val2 = p->rValue(bin, 2); //get bin value
      //if(firstEvent) cout << p->rValue(bin,"time") << endl;
      hWF->SetBinContent(bin, val2 );//set the Waveform histogram value for this bin
      if(bin < start) { //if we are still in pedestal range
        ped += val2/start;
        pedint += (0.2*(val2)/50);
        /*
        if(hit == false){//if value is beyond threshold
           oldbin = bin;
           hit = true;//seh1->Fit(&gaussFit, "E");t hit to true
           charge += .2*( val2)/50;
        }else if(hit == true && bin <= oldbin + spikewidth){ //value is back above threshold and previous bin was within a pulse
           hit = false; // change hit back to false
           charge += .2*( val2)/50;
        }
        */
        if(val2 < pedmax) pedmax = val2;//pedmax max in ped :)
      }else{//we are no longer in pedestal range
          if ((abs(pedmax)-abs(ped)) > abs(thresh)){
	           return 0;
             cout << " ";
	           cout << (abs(pedmax)-abs(ped));
	        }  //if(pedestal fluctuates) return 0;
          if(val2 < thresh+ped){//if value is beyond threshold
    	       hit = true;
             bincounter += 1;//seh1->Fit(&gaussFit, "E");t hit to true
             charge += .2*( val2)/50;
             if(val2 < max) max = val2;//look for max value of the pulse
	        }else if(hit == true){ //value is back above threshold and previous bin was within a pulse
             charge += pedint/(start/spikewidth);
             spikecounter += 1;
             bincounter += 1;
             hit = false; // change hit back to false
             charge += .2*( val2)/50;
             //integrate last bin of hit
           //Fill the two values we care about
             h1->Fill(-charge);
             h2->Fill(-max);
           //reset the counters
             charge = 0.0;
             max = 0.0;
           }//end if end of pulse
      }//end else (not ped window)
    test->SetBinContent(bin,ped);
    }//end bin for loop
    //hWFline = new TLine(0,ped,1024,ped);
    spikewidth = bincounter/spikecounter;

  }//end if packet exists
  //firstEvent = false;
  return 0;

}//end process_event
