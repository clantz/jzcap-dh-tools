#include <vector>
#include <iostream>
#include "TPaveStats.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TPad.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TSpectrum.h"

int  bunchplot(){
  gStyle->SetOptStat(0);
  gStyle->SetPalette(75);
  std::vector < TGraphErrors* > curves;
  std::vector < int > colors;
  colors.push_back(kGreen+2); colors.push_back(kCyan+2); colors.push_back(kBlue+2); colors.push_back(kMagenta+2); colors.push_back(kRed); colors.push_back(kViolet);
  string names[5];
  cout << "name: ";
  cin >> names[0]; cout << "name: "; cin >> names[1];cout << "name: ";  cin >> names[2];cout << "name: ";  cin >> names[3];cout << "name: ";  cin >> names[4];

  TCanvas *c0 = new TCanvas("Gain Curves","Gain Curves",900,700);

  const char* ext = ".root";
  const char* inDir = "~/match/";

  char* dir = gSystem->ExpandPathName(inDir);
  void* dirp = gSystem->OpenDirectory(dir);

  const char* entry;
  const char* files[100];
  Int_t n = 0;
  TString str;
  
  while((entry = (char*)gSystem->GetDirEntry(dirp))) {
    str = entry;
    if(str.EndsWith(ext))
      files[n++] = gSystem->ConcatFileName(dir, entry);
  }
  
  TMultiGraph *mg = new TMultiGraph();
  Double_t xpos[n], ypos[n];
  string labelnames[n];
  
  for( int i = 0; i < 5; i++) {
    string tempfile = names[i]+"_EstGain.root";
    files[i] = tempfile.c_str();
    Printf("file%d -> %s",i, files[i]);
    TFile *f = new TFile(files[i],"read");
    string tempname = f->GetName();
    labelnames[i] = tempname.substr(0,6);//tempname.size()-12
    f->ls();
    TCanvas *c; f->GetObject("Gain", c);
    c->ls();
    TGraphErrors *gain = ((TGraphErrors*)(c->FindObject("Graph")));
    gain->SetMarkerColor(colors.at(i));
    TF1 *fitline = ((TF1*)(gain->FindObject("gcurvefit")));
    cout << "  param0: " << fitline->GetParameter(0) << "  param1: " << fitline->GetParameter(1) << "   ";
    gain->GetPoint(0,xpos[i],ypos[i]);
    fitline->SetLineColor(colors.at(i));
    mg->Add(gain);
  }
  c0->cd();
  mg->Draw("ap");
  for( int j = 0; j < 5; j++) {
    TLatex *tex=new TLatex(xpos[j],ypos[j],Form("%s",labelnames[j].c_str()));
    tex->SetTextFont(13);
    tex->SetTextSize(18);
    tex->SetTextAlign(12);
    //tex->SetTextAngle(90);
    tex->SetTextColor(colors.at(j));
    tex->Draw();
  }
  //c->Update();
  //gPad->Update();

  return 0;
}
