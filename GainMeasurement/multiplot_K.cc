#include <vector>
#include <string>
#include <iostream>
#include "TPaveStats.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TPad.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TSpectrum.h"
#include "TMath.h"
#include "math.h"
#include <map>

int  multiplot(){
  gStyle->SetOptStat(0);
  gStyle->SetPalette(75);
  std::vector < TGraphErrors* > curves;
  string names[5];
  // cout << "name: ";
  // cin >> names[0]; cout << "name: "; cin >> names[1];cout << "name: ";  cin >> names[2];cout << "name: ";  cin >> names[3];cout << "name: ";  cin >> names[4];


  TCanvas *c0 = new TCanvas("Gain Curves","Gain Curves",900,700);

  const char* ext = ".root";
  const char* inDir = "~/match/";

  char* dir = gSystem->ExpandPathName(inDir);
  void* dirp = gSystem->OpenDirectory(dir);

  const char* entry;
  const char* files[100];
  Int_t n = 0;
  TString str;

  while((entry = (char*)gSystem->GetDirEntry(dirp))) {
    str = entry;
    if(str.EndsWith(ext))
      files[n++] = gSystem->ConcatFileName(dir, entry);
  }

  TMultiGraph *mg = new TMultiGraph();
  Double_t xpos[n], ypos[n];
  string labelnames[n];

  std::vector < string > pmtnames;
  std::vector < string > param1list;
  std:: vector < string > param2list;
  //for( int i = 0; i < 5; i++) {
  //  string tempfile = names[i]+"_EstGain.root";
  //  files[i] = tempfile.c_str();
 for (Int_t i = 0; i < n; i++) {
    Printf("file%d -> %s",i, files[i]);
    TFile *f = new TFile(files[i],"read");
    string tempname = f->GetName();
    labelnames[i] = tempname.substr(17,6);//tempname.size()-12
    pmtnames.push_back(labelnames[i]);

    f->ls();
    TCanvas *c; f->GetObject("Gain", c);
    c->ls();
    TGraphErrors *gain = ((TGraphErrors*)(c->FindObject("Graph")));
    TF1 *fitline = ((TF1*)(gain->FindObject("gcurvefit")));
    cout << "  param0: " << fitline->GetParameter(0) << "  param1: " << fitline->GetParameter(1) << "   ";
    string parameter0 = Form("%e",fitline->GetParameter(0));
    string parameter1 = Form("%f",fitline->GetParameter(1));

    param1list.push_back(parameter0);

    param2list.push_back(parameter1);


    gain->GetPoint(0,xpos[i],ypos[i]);
    gain->SetMarkerColor(i+34);
    fitline->SetLineColor(i+34);
    mg->Add(gain);

  }
  c0->cd();
  mg->Draw("ap");
/*
  for (std::vector<std::string>::iterator t = pmtnames.begin(); t != pmtnames.end(); t++) {
    std::cout << *t << std::endl; }
  for (std::vector<std::string>::iterator t = param1list.begin(); t != param1list.end(); t++) {
    std::cout << *t << std::endl; }
  for (std::vector<std::string>::iterator t = param2list.begin(); t != param2list.end(); t++) {
    std::cout << *t << std::endl; }
*/

  std::map < std::string, std::string > toyMap;
  std::map < std::string, TF1* > PMTfitFunctions;

std::cout<<'\n'<<std::endl;
for (size_t i = 0; i != pmtnames.size(); ++i){
  //double x = 0;
  int voltage = 1300;
  float test = std::stof(param2list[i])*8;
  float gainvalue = std::stof(param1list[i])*pow(voltage,test);
  std::cout<<pmtnames[i]<<std::endl;
  std::cout<< gainvalue<<std::endl;
  //std::string fitResults = Form("%e",gainvalue);
  //std::cout<< fitResults<<std::endl;
  /*toyMap[ pmtnames[i].c_str() ] = fitResults;
  //toyMap[ "RX1122" ] = fitResults;

  for (auto const& j : toyMap)
  {
    std::cout << "PMTfitMap [ \"" <<  j.first << "\" ] = "  // string (key)
              << j.second << ";"// fit function's value
              << std::endl;
    PMTfitFunctions[ j.first ] = new TF1(j.first.c_str(),j.second.c_str(),1000,1500);

  }
  for (auto const& j : PMTfitFunctions)
  {
    std::cout << "PMTfitFunctions [ \"" <<  j.first << "\" ] = "  // string (key)
              << j.second->Eval(1300) << ";"// fit function's value
              << std::endl;
  }

  std::cout << "PMTfitFunctions [ RX1122 ] = "
            //<< PMTfitFunctions[ "RX1122" ]->Eval(1300) << ";"// fit function's value
            << std::endl;
return 0;
}*/}
  //for( int j = 0; j < 5; j++) {
    for (Int_t j = 0; j < n; j++) {
    TLatex *tex=new TLatex(xpos[j],ypos[j],Form("%s",labelnames[j].c_str()));
    tex->SetTextFont(13);
    tex->SetTextSize(14);
    tex->SetTextAlign(12);
    //tex->SetTextAngle(90);
    tex->SetTextColor(j+34);
    //tex->SetTitle("PMT Gain");
    tex->Draw();
  }
  //c->Update();
  //gPad->Update();

  return 0;
}
