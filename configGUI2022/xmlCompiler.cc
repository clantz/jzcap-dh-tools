/****************************************************
*
* Alignment file compiler for the 2021 JZCaP Test Beam
*
* Usage:
* ./compileXML /path/to/files outputFileName
*
* This program will find all .xml files in a given directory
* and compile the contents into a single file
*
****************************************************/


#include <iostream>
#include <fstream>

#include "TString.h"
#include "TSystem.h"
#include "TSystemDirectory.h"
#include "TList.h"
#include "TFile.h"
#include "TTree.h"
#include "TH2F.h"

#include "XMLSettingsReader.hh"
#include "T4XMLWriter.hh"

using namespace std;

namespace {
  void PrintUsage() {
    cerr << " Usage: " << endl;
    cerr << " compileXML /path/to/input/files/ /output/dir/ outputFileName"
           << endl;
  }
}

void doConfigs(string inputDir, string _outFile);
Config2022* LoadConfigFile(string _inFile );
vector< TString > list_files(const char *dirname="C:/root/folder/", const char *ext=".root");
void setupTree(Config2022* _buffer);

vector<int> wrongRun = { 69806, 69807, 69858, 69879 };
vector<int> rightRun = { 69827, 69828, 69860, 69880 };

vector< string > p1detTypes = {"8-1 HAD1","8-1 EM","8-1 HAD2","8-1 HAD3","1-2 HAD3","1-2 HAD2","1-2 HAD1"};
vector< float  > p1Tbl      = { 278.23,    183.23,    89.73,     -2.77,    -96.27,   -189.27,   -281.77};

vector< string > p2detTypes = {"Arm 8-1", "Arm 1-2"};

vector< string > p3detTypes = {"Arm 8-1", "Arm 1-2", "1-2 HAD3"};
vector< float  > p3tbl      = {   206,      -209,      -57.89  };

TFile *f;
TTree *t;



/*
 * Compile all .xml files containing alignment
 */
int main(int argc, char** argv){

  string input, outputFile, outputDir;
  TString testStr;
  if(argc == 4){
    input = argv[1];
    outputDir = argv[2];
    outputFile = argv[3];
  }else{
    cout << "Undefined behavior: Exiting" << endl;
    return -1;
  }

  f = new TFile(Form("%s%s.root",outputDir.c_str(),outputFile.c_str()), "recreate");
  doConfigs(input, Form("%s%s.xml",outputDir.c_str(),outputFile.c_str()) );

}//end main


void doConfigs(string inputDir, string _outFile){

    t = new TTree("TB2022config","TB2022config");
    Config2022* buffer = new Config2022();
    setupTree(buffer);

    //Retrieve alignments from each file into alignment objects
    vector< Config2022* > entries;
    vector< TString > file_list = list_files( inputDir.c_str(), ".xml");
    for( TString file : file_list ){
      entries.push_back( LoadConfigFile( (inputDir + file).Data() ) );
    }//end file loop

    //Create a new file with all of the alignments
    T4XMLWriter *myWriter = new T4XMLWriter();
    for( int config = 0; config < entries.size(); config++){
      if( entries[config] == NULL ){
        cout << "Error reading config file" << endl;
        return;
      }
      for(int check = 0; check < wrongRun.size(); check ++){
        if(entries[config]->run == wrongRun[check]){
          cout << "Changing run " << entries[config]->run<< " to " << rightRun[check] << endl;
          entries[config]->run = rightRun[check];
          break;
        }
      }

      if( entries[config]->phase == 211.57 ) entries[config]->phase = 2;
      if( entries[config]->phase == 1 ){
        for( int det = 0; det < p1detTypes.size(); det++ ){
          if( fabs(entries[config]->x_det_table - p1Tbl[det]) > 10 ){
            entries[config]->detector = p1detTypes[det];
          }
        }
      }else if( entries[config]->phase == 2  ){
        for( int det = 0; det < p1detTypes.size(); det++ ){
          entries[config]->detector = ( entries[config]->x_det_table < 0 ) ? "Arm 1-2" : "Arm 8-1";
        }
      }else if( entries[config]->phase == 3 ){
        for( int det = 0; det < p3detTypes.size(); det++ ){
          if( fabs(entries[config]->x_det_table - p3tbl[det]) > 10 ){
            entries[config]->detector = p3detTypes[det];
          }
        }
      }
      if(entries[config]->detector == "true"){
        cout << "Run " << entries[config]->run << " table " << entries[config]->x_trig_table << endl;
      }
      myWriter->addNode("Alignment");
      myWriter->createDataNode("Alignment",config,"Shift_leader", entries[config]->shiftLeader   );
      myWriter->createDataNode("Alignment",config,"run",          entries[config]->run           );
      myWriter->createDataNode("Alignment",config,"phase",        entries[config]->phase         );
      myWriter->createDataNode("Alignment",config,"good_run",     entries[config]->good          );
      myWriter->createDataNode("Alignment",config,"x_table",      entries[config]->x_det_table   );
      myWriter->createDataNode("Alignment",config,"y_table",      entries[config]->y_det_table   );
      myWriter->createDataNode("Alignment",config,"x_trigger",    entries[config]->x_trig_table  );
      myWriter->createDataNode("Alignment",config,"y_trigger",    entries[config]->y_trig_table  );
      myWriter->createDataNode("Alignment",config,"beam_energy",  entries[config]->beam_energy   );
      myWriter->createDataNode("Alignment",config,"beam_type",    entries[config]->beam_type     );
      myWriter->createDataNode("Alignment",config,"Detector",     entries[config]->detector      );
      myWriter->createDataNode("Alignment",config,"ZDC_HV",       entries[config]->ZDC_HV        );
      myWriter->createDataNode("Alignment",config,"ZDC_b1",       entries[config]->ZDC_b1        );
      myWriter->createDataNode("Alignment",config,"ZDC_b2",       entries[config]->ZDC_b2        );
      myWriter->createDataNode("Alignment",config,"ZDC_b3",       entries[config]->ZDC_b3        );
      myWriter->createDataNode("Alignment",config,"RPD_HV1",      entries[config]->RPD_HV1       );
      myWriter->createDataNode("Alignment",config,"RPD_HV2",      entries[config]->RPD_HV2       );
      myWriter->createDataNode("Alignment",config,"RPD_HV3",      entries[config]->RPD_HV3       );
      myWriter->createDataNode("Alignment",config,"RPD_HV4",      entries[config]->RPD_HV4       );
      myWriter->createDataNode("Alignment",config,"EM_HV",        entries[config]->EM_HV         );

      buffer->shiftLeader = entries[config]->shiftLeader;
      buffer->run = entries[config]->run;
      buffer->phase = entries[config]->phase;
      buffer->good = entries[config]->good;
      buffer->x_det_table = entries[config]->x_det_table;
      buffer->y_det_table = entries[config]->y_det_table;
      buffer->x_trig_table = entries[config]->x_trig_table;
      buffer->y_trig_table = entries[config]->y_trig_table;
      buffer->beam_energy = entries[config]->beam_energy;
      buffer->beam_type = entries[config]->beam_type;
      buffer->detector = entries[config]->detector;
      buffer->ZDC_HV = entries[config]->ZDC_HV;
      buffer->ZDC_b1 = entries[config]->ZDC_b1;
      buffer->ZDC_b2 = entries[config]->ZDC_b2;
      buffer->ZDC_b3 = entries[config]->ZDC_b3;
      buffer->RPD_HV1 = entries[config]->RPD_HV1;
      buffer->RPD_HV2 = entries[config]->RPD_HV2;
      buffer->RPD_HV3 = entries[config]->RPD_HV3;
      buffer->RPD_HV4 = entries[config]->RPD_HV4;
      buffer->EM_HV = entries[config]->EM_HV;

      t->Fill();
    }

    f->Write();
    f->Close();
    myWriter->createFile( _outFile.c_str() );
    delete myWriter;

}


/**
 * @brief Reads the .xml configuration file and load characteristics for all the channels, immediately sorted into detectors objects
 * @param _inFile
 */
Config2022* LoadConfigFile(string _inFile ){

    XMLSettingsReader *XMLparser = new XMLSettingsReader();

    if (!XMLparser->parseFile(_inFile)) {
            cerr << " Data Reader could not parse file : " << _inFile << endl;
            return NULL;
    }

    Config2022 *cfg = new Config2022();
    if(XMLparser->getBaseNodeCount("Alignment") != 1){
      cout << "Wrong number of nodes in " << _inFile << endl;
      return NULL;
    }
    for (unsigned int i = 0; i < XMLparser->getBaseNodeCount("Alignment"); i++) {
        XMLparser->getChildValue("Alignment",i,"Shift_leader",  cfg->shiftLeader   );
        XMLparser->getChildValue("Alignment",i,"run",           cfg->run           );
        XMLparser->getChildValue("Alignment",i,"phase",         cfg->phase         );
        XMLparser->getChildValue("Alignment",i,"good_run",      cfg->good          );
        XMLparser->getChildValue("Alignment",i,"x_table",       cfg->x_det_table   );
        XMLparser->getChildValue("Alignment",i,"y_table",       cfg->y_det_table   );
        XMLparser->getChildValue("Alignment",i,"x_trigger",     cfg->x_trig_table  );
        XMLparser->getChildValue("Alignment",i,"y_trigger",     cfg->y_trig_table  );
        XMLparser->getChildValue("Alignment",i,"beam_energy",   cfg->beam_energy   );
        XMLparser->getChildValue("Alignment",i,"beam_type",     cfg->beam_type     );
        XMLparser->getChildValue("Alignment",i,"Detector",      cfg->detector      );
        XMLparser->getChildValue("Alignment",i,"ZDC_HV",        cfg->ZDC_HV        );
        XMLparser->getChildValue("Alignment",i,"ZDC_b1",        cfg->ZDC_b1        );
        XMLparser->getChildValue("Alignment",i,"ZDC_b2",        cfg->ZDC_b2        );
        XMLparser->getChildValue("Alignment",i,"ZDC_b3",        cfg->ZDC_b3        );
        XMLparser->getChildValue("Alignment",i,"RPD_HV1",       cfg->RPD_HV1       );
        XMLparser->getChildValue("Alignment",i,"RPD_HV2",       cfg->RPD_HV2       );
        XMLparser->getChildValue("Alignment",i,"RPD_HV3",       cfg->RPD_HV3       );
        XMLparser->getChildValue("Alignment",i,"RPD_HV4",       cfg->RPD_HV4       );
        XMLparser->getChildValue("Alignment",i,"EM_HV",         cfg->EM_HV         );
    }

    if(cfg == NULL) cout << "WARNING: ALIGNMENT NOT FOUND!!!" << endl;
    delete XMLparser; XMLparser = NULL;
    return cfg;

}


/**
* @brief Sets the branches of the output ttree with the buffer config object
*/
void setupTree(Config2022* _buffer){

  t->Branch("shiftLeader",  &_buffer->shiftLeader      );
  t->Branch("run",          &_buffer->run              );
  t->Branch("phase",        &_buffer->phase            );
  t->Branch("good",         &_buffer->good             );
  t->Branch("x_det_table",  &_buffer->x_det_table      );
  t->Branch("y_det_table",  &_buffer->y_det_table      );
  t->Branch("x_trig_table", &_buffer->x_trig_table     );
  t->Branch("y_trig_table", &_buffer->y_trig_table     );
  t->Branch("beam_energy",  &_buffer->beam_energy      );
  t->Branch("beam_type",    &_buffer->beam_type        );
  t->Branch("detector",     &_buffer->detector         );
  t->Branch("ZDC_HV",       &_buffer->ZDC_HV           );
  t->Branch("ZDC_b1",       &_buffer->ZDC_b1           );
  t->Branch("ZDC_b2",       &_buffer->ZDC_b2           );
  t->Branch("ZDC_b3",       &_buffer->ZDC_b3           );
  t->Branch("RPD_HV1",      &_buffer->RPD_HV1          );
  t->Branch("RPD_HV2",      &_buffer->RPD_HV2          );
  t->Branch("RPD_HV3",      &_buffer->RPD_HV3          );
  t->Branch("RPD_HV4",      &_buffer->RPD_HV4          );
  t->Branch("EM_HV",        &_buffer->EM_HV            );
}


/**
 * @brief Lists all files in a given directory with the given extension
 */
vector< TString > list_files(const char *dirname, const char *ext) {
  vector< TString > list;
  TSystemDirectory dir(dirname, dirname);
  TList *files = dir.GetListOfFiles();

  if (files) {
    TSystemFile *file;
    TString fname;
    TIter next(files);
    while ((file=(TSystemFile*)next())) {
      fname = file->GetName();
      if (!file->IsDirectory() && fname.EndsWith(ext)) {
        // cout << fname.Data() << endl;
        list.push_back( fname );
      }
    }
  }
  return list;
}

/**
 * @brief Gets one data point from a csv line then erases that point from the line
 */
string getDataPoint( string &input, size_t spaces, string delim ){
  size_t firstComma = input.find_first_of(delim.c_str());
  string buffer = input.substr(0, firstComma );
  input.erase(0,firstComma+spaces);
  if(buffer == "") buffer = "nan";
  return buffer;
}
