#include "MyMainFrame.hh"
#include "TApplication.h"
#include "TROOT.h"
#include "TGClient.h"

int main(int argc, char** argv){
  TApplication theApp("App",&argc,argv);
  MyMainFrame *mf = new MyMainFrame(gClient->GetRoot(),800,120);
  if(argc > 1) mf->setDebugValues(); 
  theApp.Run();
  return 0;
}
