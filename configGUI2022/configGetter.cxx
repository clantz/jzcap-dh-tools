#include <TFile.h>
#include <TTree.h>

struct Config{

     /** Shift Leader */
     std::string shiftLeader;
     /** Run number being analyzed **/
     int run;
     /** Phase of test beam **/
     int phase;
     /** If the run was good */
     bool good;
     /** X position of the NIKHEF Table **/
     float x_det_table;
     /** Y position of the NIKHEF Table **/
     float y_det_table;
     /** X position of the Trigger Table **/
     float x_trig_table;
     /** Y position of the Trigger Table **/
     float y_trig_table;
     /** Beam type - can be p or e in H2 2021 **/
     std::string beam_type;
     /** Beam energy **/
     float beam_energy;
     /** Absorber thickness **/
     float absorber;
     /** Detector in the beam **/
     std::string detector;
     /** Main high voltage setting for the ZDC */
     float ZDC_HV;
     /** High voltage booster 1 setting for the ZDC */
     float ZDC_b1;
     /** High voltage booster 2 setting for the ZDC */
     float ZDC_b2;
     /** High voltage booster 3 setting for the ZDC */
     float ZDC_b3;
     /** High voltage setting for RPD channel 1-4*/
     float RPD_HV1;
     /** High voltage setting for RPD channel 1-4*/
     float RPD_HV2;
     /** High voltage setting for RPD channel 1-4*/
     float RPD_HV3;
     /** High voltage setting for RPD channel 1-4*/
     float RPD_HV4;
     /** High voltage setting for the prototype EM*/
     float EM_HV;

};


class configGetter{

  public:
    configGetter() : f(nullptr), t(nullptr);
    configGetter(TString fileName, TString treeName = "TB2023config"){
        f = new TFile(fileName.Data(), "read");
        if(!f.IsOpen()){
            cout << fileName << " could not be opened" << endl;
            delete f;
            f = nullptr;
        }else{
            t = (TTree*)f->Get(treeName.Data());
        }
    };

    ~configGetter{
        if(f){
            if(f->isOpen()) f->Close();
            delete f;
        }
    }

    Config getRun(int runNo){

        if(!f && !t){
            std::runtime_error("configGetter: No file or tree found when getting run");
        }
        
        Config cfg;

        t->SetBranchAddress("run",          &cfg.run              );

        int entry;
        for(entry = 0; entry < t->GetEntries(); entry++){
            t->GetEntry(entry);
            if(cfg.run == runNo) break;
        }

        string *sl=0,*det=0,*beam=0;
        t->SetBranchAddress("shiftLeader",  &sl                   );
        t->SetBranchAddress("phase",        &cfg.phase            );
        t->SetBranchAddress("good",         &cfg.good             );
        t->SetBranchAddress("x_det_table",  &cfg.x_det_table      );
        t->SetBranchAddress("y_det_table",  &cfg.y_det_table      );
        t->SetBranchAddress("x_trig_table", &cfg.x_trig_table     );
        t->SetBranchAddress("y_trig_table", &cfg.y_trig_table     );
        t->SetBranchAddress("beam_energy",  &cfg.beam_energy      );
        t->SetBranchAddress("beam_type",    &beam                 );
        t->SetBranchAddress("detector",     &det                  );
        t->SetBranchAddress("ZDC_HV",       &cfg.ZDC_HV           );
        t->SetBranchAddress("ZDC_b1",       &cfg.ZDC_b1           );
        t->SetBranchAddress("ZDC_b2",       &cfg.ZDC_b2           );
        t->SetBranchAddress("ZDC_b3",       &cfg.ZDC_b3           );
        t->SetBranchAddress("RPD_HV1",      &cfg.RPD_HV1          );
        t->SetBranchAddress("RPD_HV2",      &cfg.RPD_HV2          );
        t->SetBranchAddress("RPD_HV3",      &cfg.RPD_HV3          );
        t->SetBranchAddress("RPD_HV4",      &cfg.RPD_HV4          );
        t->SetBranchAddress("EM_HV",        &cfg.EM_HV            );

        t->GetEntry(entry);

        cfg.shiftLeader = *sl;
        cfg.beam_type = *beam;
        cfg.detector = *det;

        return cfg;
    };

    void openFile(TString fileName, TString treeName = "TB2023config"){
        f = new TFile(fileName.Data(), "read");
        if(!f.IsOpen()){
            cout << fileName << " could not be opened" << endl;
            delete f;
            f = nullptr;
        }else{
            t = (TTree*)f->Get(treeName.Data());
        }
    }


    bool fileIsOpen(){
        if(!f){
            return false;
        }else{
            return f->IsOpen();
        }
    }


  private:
    TFile *f;
    TTree *t;
}