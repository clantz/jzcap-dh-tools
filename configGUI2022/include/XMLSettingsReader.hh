/** @file XMLSettingsReader
 *  @brief Function prototypes for XMLSettingsReader
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef XMLSETTINGSREADER_HH
#define XMLSETTINGSREADER_HH

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include <string>
#include <vector>
#include <sstream>
#include <iostream>

using namespace xercesc;

class Config2022{

  public:

     /** Shift Leader */
     std::string shiftLeader;
     /** Run number being analyzed **/
     int run;
     /** Phase of test beam **/
     int phase;
     /** If the run was good */
     bool good;
     /** X position of the NIKHEF Table **/
     double x_det_table;
     /** Y position of the NIKHEF Table **/
     double y_det_table;
     /** X position of the Trigger Table **/
     double x_trig_table;
     /** Y position of the Trigger Table **/
     double y_trig_table;
     /** Beam type - can be p or e in H2 2021 **/
     std::string beam_type;
     /** Beam energy **/
     double beam_energy;
     /** Absorber thickness **/
     double absorber;
     /** Detector in the beam **/
     std::string detector;
     /** Main high voltage setting for the ZDC */
     double ZDC_HV;
     /** High voltage booster 1 setting for the ZDC */
     double ZDC_b1;
     /** High voltage booster 2 setting for the ZDC */
     double ZDC_b2;
     /** High voltage booster 3 setting for the ZDC */
     double ZDC_b3;
     /** High voltage setting for RPD channel 1-4*/
     double RPD_HV1;
     /** High voltage setting for RPD channel 1-4*/
     double RPD_HV2;
     /** High voltage setting for RPD channel 1-4*/
     double RPD_HV3;
     /** High voltage setting for RPD channel 1-4*/
     double RPD_HV4;
     /** High voltage setting for the prototype EM*/
     double EM_HV;

};

class XMLSettingsReader {

 public :
      XMLSettingsReader( void );
      virtual ~XMLSettingsReader( void );

      int getBaseNodeCount(std::string _nodeName);
      int getNodeChildCount(std::string _nodeName, int _nodeNumber, std::string _childName);

      std::string getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode);

      void getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode, std::string &_retVal);
      void getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode, bool& _retVal);
      void getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode, double& _retVal);
      void getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode, int& _retVal);

      bool parseFile(std::string _fileName);

 private :
      XercesDOMParser* m_DOMParser;
      DOMElement* m_rootNode;
};

#endif
