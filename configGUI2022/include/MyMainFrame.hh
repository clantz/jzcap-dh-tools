#ifndef MYMAINFRAME_HH_
#define MYMAINFRAME_HH_

#include <TGClient.h>
#include <TGFrame.h>
#include <TGTab.h>
#include <TGButton.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGTextEdit.h>
#include <TGStatusBar.h>
#include <TGMenu.h>
#include <TTimer.h>
#include <TGDockableFrame.h>
#include <TTree.h>

#include <vector>
#include <string>

class TGWindow;
class TRootEmbeddedCanvas;


struct Config{

     /** Shift Leader */
     std::string shiftLeader;
     /** Run number being analyzed **/
     int run;
     /** Phase of test beam **/
     int phase;
     /** If the run was good */
     bool good;
     /** X position of the NIKHEF Table **/
     float x_det_table;
     /** Y position of the NIKHEF Table **/
     float y_det_table;
     /** X position of the Trigger Table **/
     float x_trig_table;
     /** Y position of the Trigger Table **/
     float y_trig_table;
     /** Beam type - can be p or e in H2 2021 **/
     std::string beam_type;
     /** Beam energy **/
     float beam_energy;
     /** Absorber thickness **/
     float absorber;
     /** Detector in the beam **/
     std::string detector;
     /** Main high voltage setting for the ZDC */
     float ZDC_HV;
     /** High voltage booster 1 setting for the ZDC */
     float ZDC_b1;
     /** High voltage booster 2 setting for the ZDC */
     float ZDC_b2;
     /** High voltage booster 3 setting for the ZDC */
     float ZDC_b3;
     /** High voltage setting for RPD channel 1-4*/
     float RPD_HV1;
     /** High voltage setting for RPD channel 1-4*/
     float RPD_HV2;
     /** High voltage setting for RPD channel 1-4*/
     float RPD_HV3;
     /** High voltage setting for RPD channel 1-4*/
     float RPD_HV4;
     /** High voltage setting for the prototype EM*/
     float EM_HV;

};

enum ECommandIdentifiers {
   M_FILE_OPEN,
   M_FILE_SAVEAS,
   M_FILE_EXIT
};

class MyMainFrame : public TGMainFrame {

private:
   TGDockableFrame *fMenuDock;
   TGMenuBar    *fMenuBar;
   TGLayoutHints *fMenuBarLayout, *fMenuBarItemLayout, *fMenuBarHelpLayout;
   TGPopupMenu  *fMenuFile;
   TGTextButton *writeBtn, *exit;
   TGNumberEntry *Phase, *NumTblX, *NumTblY, *NumTrgX, *NumTrgY, *ZDChv, *ZDCb1, *ZDCb2, *ZDCb3, *RPDhv1, *RPDhv2, *RPDhv3, *RPDhv4, *UEMhv, *NumBeamE, *RunNum;
   TGTextEntry  *detTableIP;
   TGComboBox *Beam, *Det, *Shifter;
   TGCheckButton *badRun;
   TGTextEdit *msgBox;
   TGStatusBar *StatusBar;
   TTimer *Timer;
   TString m_rootFileName;
   bool m_rootFileOpen;
   int currentRunNo;

public:
   MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h);
   virtual ~MyMainFrame();

   void HandleMenu(Int_t id);
   void endRun();
   void ZDChvSet();
   void errorMessage(TString _title, TString _message);
   void fillFields(Config cfg);
   void WriteXML( Config cfg );
   void openRootFile(TString fileName);
   void createRootFile(Config cfg);
   void updateRootFile(Config cfg);
   bool validateIpAddress(const std::string &ipAddress);
   void setDebugValues();

   Config getCurrentConfig(int runNo);

   ClassDef(MyMainFrame, 0)

   std::vector<TString> m_detTypes;
   std::vector<TString> m_shiftLeaders;
};


#endif /* MYMAINFRAME_HH_ */
