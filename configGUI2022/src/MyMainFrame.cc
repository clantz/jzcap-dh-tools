/// \file
/// \ingroup tutorial_gui
/// This macro gives an example of different buttons' layout.
/// To run it do either:
/// ~~~
/// .x buttonsLayout.C
/// .x buttonsLayout.C++
/// ~~~
///
/// \macro_code
///
/// \author Chad Lantz 8 August 2021



#include <TGButton.h>
#include <TGLabel.h>
#include <TGMsgBox.h>
#include <TGFileDialog.h>
#include <TSystem.h>
#include <TDatime.h>
#include <TFile.h>


#include <iostream>
#include <sstream>
#include <vector>
#include <arpa/inet.h>

#include "MyMainFrame.hh"
#include "T4XMLWriter.hh"


using namespace std;


MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h) :
    m_rootFileName("configs.root"),
    m_rootFileOpen(false),
    TGMainFrame(p, w, h)
{


   SetCleanup(kDeepCleanup);


     // Create menubar and popup menus. The hint objects are used to place
   // and group the different menu widgets with respect to eachother.
   fMenuBar = new TGMenuBar(this, 35, 50, kHorizontalFrame);

   fMenuFile = new TGPopupMenu(gClient->GetRoot());
   fMenuFile->AddEntry(" &Open...\tCtrl+O", M_FILE_OPEN, 0,
                       gClient->GetPicture("bld_open.png"));
   fMenuFile->AddEntry("&SaveAs\tCtrl+Shift+S", M_FILE_SAVEAS);
   fMenuFile->AddSeparator();
   fMenuFile->AddEntry(" E&xit\tCtrl+Q", M_FILE_EXIT, 0,
                       gClient->GetPicture("bld_exit.png"));
   fMenuFile->Connect("Activated(Int_t)", "MyMainFrame", this,
                      "HandleMenu(Int_t)");


   fMenuBar->AddPopup("&File", fMenuFile, new TGLayoutHints(kLHintsTop|kLHintsLeft,
                                                            0, 4, 0, 0));

   AddFrame(fMenuBar, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 5));


  m_detTypes = {"Arm 8-1",
                "Arm 1-2",
                "Arm 8-1 + Proto EM",
                "Arm 1-2 + Proto EM",};


  m_shiftLeaders = {"Brian Cole",
                    "Peter Steinberg",
                    "Riccardo Longo",
                    "Bill Yin",
                    "Chad Lantz",
                    "Yftach Moyal",
                    "Matthew Hoppesch",
                    "Mason Housenga",
                    "Blair Seidlitz"};


   // Create a container frames containing buttons

   TGNumberFormat::EStyle style = TGNumberFormat::kNESReal;
   TGNumberFormat::EAttribute attr = TGNumberFormat::kNEAAnyNumber;
   TGNumberFormat::ELimit lim;

   //Top row frame
   TGCompositeFrame *cframe1 = new TGCompositeFrame(this, w, 25,
     kHorizontalFrame );

    /////////////////////////////////
    /////      Configuration
    /////////////////////////////////
    int numberEntryWidth = 20;

   TGGroupFrame *configFrame = new TGGroupFrame(cframe1, "Configuration", kVerticalFrame);
   configFrame->SetTitlePos(TGGroupFrame::kLeft); // right aligned
   configFrame->SetLayoutManager(new TGMatrixLayout(configFrame, 0, 2, 10));
   cframe1->AddFrame(configFrame,new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 5, 5, 5, 5));
   // cframe1->AddFrame(configFrame,new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 5, 5, 5, 5));

   //Phase
   configFrame->AddFrame(new TGLabel(configFrame, new TGString("Phase")), new TGLayoutHints(kLHintsTop | kLHintsRight | kLHintsExpandX,2, 2, 2, 2) );
   Phase = new TGNumberEntry(configFrame, 1, numberEntryWidth);
   Phase->SetFormat( style, attr );
   Phase->SetLimitValues( 1, 3 );
   configFrame->AddFrame(Phase, new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 2, 2, 2, 2) );

   //Detectors
   configFrame->AddFrame(new TGLabel(configFrame, new TGString("Detector")), new TGLayoutHints(kLHintsTop | kLHintsRight | kLHintsExpandX,2, 2, 2, 2) );
   Det = new TGComboBox(configFrame);
   Det->SetWidth(numberEntryWidth);
   Det->AddEntry("",0);//Add a blank entry for default
   for(int type = 0; type < m_detTypes.size(); type++){
     Det->AddEntry(m_detTypes[type].Data(),type+1);
   }
   Det->Resize(100,20);
   configFrame->AddFrame(Det, new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 2, 2, 2, 2) );

   //Table IP address
    configFrame->AddFrame(new TGLabel(configFrame, new TGHotString("Det Table IP")));
    TGTextBuffer *tbuf = new TGTextBuffer(15);
    // tbuf->AddText(0, "0.0.0.0");
    detTableIP = new TGTextEntry(configFrame, tbuf);
    // detTableIP->Resize(50, detTableIP->GetDefaultHeight());
    detTableIP->SetFont("-adobe-courier-bold-r-*-*-14-*-*-*-*-*-iso8859-1");
    configFrame->AddFrame(detTableIP);

   //Table X
   configFrame->AddFrame(new TGLabel(configFrame, new TGString("Table X: [mm]")), new TGLayoutHints(kLHintsTop | kLHintsRight | kLHintsExpandX,2, 2, 2, 2) );
   NumTblX = new TGNumberEntry(configFrame, 1, numberEntryWidth);
   NumTblX->SetFormat( style, attr );
   NumTblX->SetLimitValues( 1, 10 );
   configFrame->AddFrame(NumTblX, new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 2, 2, 2, 2) );


   //Table Y
   configFrame->AddFrame(new TGLabel(configFrame, new TGString("Table Y: [mm]")), new TGLayoutHints(kLHintsTop | kLHintsRight | kLHintsExpandX,2, 2, 2, 2) );
   NumTblY= new TGNumberEntry(configFrame, 1, numberEntryWidth);
   NumTblY->SetFormat( style, attr );
   NumTblY->SetLimitValues( 1, 10 );
   configFrame->AddFrame(NumTblY, new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 2, 2, 2, 2) );

   //Trigger X
   NumTrgX = new TGNumberEntry(configFrame, 1, numberEntryWidth);
   configFrame->AddFrame(new TGLabel(configFrame, new TGString("Trigger X: [mm]")), new TGLayoutHints(kLHintsTop | kLHintsRight | kLHintsExpandX,2, 2, 2, 2) );
   NumTrgX->SetFormat( style, attr );
   NumTrgX->SetLimitValues( 1, 10 );
   configFrame->AddFrame(NumTrgX, new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 2, 2, 2, 2) );

   //Trigger Y
   NumTrgY = new TGNumberEntry(configFrame, 1, numberEntryWidth);
   configFrame->AddFrame(new TGLabel(configFrame, new TGString("Trigger Y: [mm]")), new TGLayoutHints(kLHintsTop | kLHintsRight | kLHintsExpandX,2, 2, 2, 2) );
   NumTrgY->SetFormat( style, attr );
   NumTrgY->SetLimitValues( 1, 10 );
   configFrame->AddFrame(NumTrgY, new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 2, 2, 2, 2) );

   //Beam Type
   Beam = new TGComboBox(configFrame,1);//2nd argument is ID number
   Beam->SetWidth(numberEntryWidth);
   configFrame->AddFrame(new TGLabel(configFrame, new TGString("Beam Type:")), new TGLayoutHints(kLHintsTop | kLHintsRight | kLHintsExpandX,2, 2, 2, 2) );
   Beam->AddEntry("electron",1);
   Beam->AddEntry("proton",2);
   configFrame->AddFrame(Beam, new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 2, 2, 2, 2) );
   Beam->Resize(100,20);

   //Beam Energy
   NumBeamE = new TGNumberEntry(configFrame, 1, numberEntryWidth);
   configFrame->AddFrame(new TGLabel(configFrame, new TGString("Beam Energy: [GeV]")), new TGLayoutHints(kLHintsTop | kLHintsRight | kLHintsExpandX,2, 2, 2, 2) );
   NumBeamE->SetFormat( style, attr );
   NumBeamE->SetLimitValues( 1, 10 );
   configFrame->AddFrame(NumBeamE, new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 2, 2, 2, 2) );


   // AddFrame(cframe1, new TGLayoutHints(kLHintsCenterX, 2, 2, 5, 1));

   /////////////////////////////////
   /////      High voltage
   /////////////////////////////////


   //HV box
   TGGroupFrame *hvFrame = new TGGroupFrame(cframe1, "HIGH VOLTAGE", kVerticalFrame);
   hvFrame->SetTitlePos(TGGroupFrame::kLeft); // right aligned
   cframe1->AddFrame(hvFrame,new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 5, 5, 5, 5));

   // 2 column, n rows
   hvFrame->SetLayoutManager(new TGMatrixLayout(hvFrame, 0, 2, 10));


   //ZDC HV
   hvFrame->AddFrame(new TGLabel(hvFrame, new TGString("ZDC HV:")));
   ZDChv = new TGNumberEntry(hvFrame, 1, 12);
   ZDChv->SetFormat( TGNumberFormat::kNESInteger, TGNumberFormat::kNEAAnyNumber );
   ZDChv->SetLimitValues( 0, -2000 );
   ZDChv->Connect("ValueSet(Long_t)", "MyMainFrame", this, "ZDChvSet()");
   hvFrame->AddFrame(ZDChv, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );


   hvFrame->AddFrame(new TGLabel(hvFrame, new TGString("ZDC Booster 1: "))); //Label
   ZDCb1 = new TGNumberEntry(hvFrame, 1, 12);
   ZDCb1->SetState(false);
  //  ZDCb1->Connect("ValueSet(Long_t)", "MyMainFrame", this, "ZDChvSet()");
   hvFrame->AddFrame(ZDCb1, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );


   hvFrame->AddFrame(new TGLabel(hvFrame, new TGString("ZDC Booster 2: "))); //Label
   ZDCb2 = new TGNumberEntry(hvFrame, 1, 12);
   ZDCb2->SetState(false);
  //  ZDCb2->Connect("ValueSet(Long_t)", "MyMainFrame", this, "ZDChvSet()");
   hvFrame->AddFrame(ZDCb2, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );


   hvFrame->AddFrame(new TGLabel(hvFrame, new TGString("ZD Booster 3: "))); //Label
   ZDCb3 = new TGNumberEntry(hvFrame, 1, 12);
   ZDCb3->SetState(false);
  //  ZDCb3->Connect("ValueSet(Long_t)", "MyMainFrame", this, "ZDChvSet()");
   hvFrame->AddFrame(ZDCb3, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );



   //RPD HV
   hvFrame->AddFrame(new TGLabel(hvFrame, new TGString("RPD HV1:")));
   RPDhv1= new TGNumberEntry(hvFrame, 1, 12);
   hvFrame->AddFrame(RPDhv1, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   hvFrame->AddFrame(new TGLabel(hvFrame, new TGString("RPD HV2:")));
   RPDhv2= new TGNumberEntry(hvFrame, 1, 12);
   hvFrame->AddFrame(RPDhv2, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   hvFrame->AddFrame(new TGLabel(hvFrame, new TGString("RPD HV3:")));
   RPDhv3= new TGNumberEntry(hvFrame, 1, 12);
   hvFrame->AddFrame(RPDhv3, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   hvFrame->AddFrame(new TGLabel(hvFrame, new TGString("RPD HV4:")));
   RPDhv4= new TGNumberEntry(hvFrame, 1, 12);
   hvFrame->AddFrame(RPDhv4, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );


   //EM Prototype HV
   hvFrame->AddFrame(new TGLabel(hvFrame, new TGString("EM Prototype HV:")));
   UEMhv= new TGNumberEntry(hvFrame, 1, 12);
   hvFrame->AddFrame(UEMhv, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );



   AddFrame(cframe1, new TGLayoutHints( kLHintsCenterX | kLHintsExpandX, 2, 2, 5, 1));


   /////////////////////////////////
   /////      Middle row (text box)
   /////////////////////////////////
   TGHorizontalFrame *cframe2 = new TGHorizontalFrame(this, 0, 0, 0 );
   // TGCompositeFrame *cframe2 = new TGCompositeFrame(this, 550, 20, kHorizontalFrame );

   msgBox = new TGTextEdit(cframe2, w, h, kSunkenFrame | kDoubleBorder);
   cframe2->AddFrame(msgBox, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX | kLHintsExpandY, 3, 3, 3, 3) );


   //Shifter
   TGCompositeFrame *shiftframe = new TGVerticalFrame(cframe2, 0, 0, 0);


   shiftframe->AddFrame(new TGLabel(shiftframe, new TGString("Shift Leader")), new TGLayoutHints( kLHintsExpandX,2, 2, 2, 2) );
   Shifter = new TGComboBox(shiftframe);
   Shifter->AddEntry("",0);//Add a blank entry for default
   for(int name = 0; name < m_shiftLeaders.size(); name++){
     Shifter->AddEntry(m_shiftLeaders[name].Data(),name+1);
   }
   Shifter->Resize(100,20);
   shiftframe->AddFrame(Shifter, new TGLayoutHints( kFixedWidth, 2, 2, 2, 2) );

   //Add a gap before the bad run box
   shiftframe->AddFrame(new TGLabel(shiftframe, new TGString("")), new TGLayoutHints( kLHintsExpandX,2, 2, 2, 2) );

   badRun  = new TGCheckButton(shiftframe, new TGHotString("Bad Run"), -1);
   shiftframe->AddFrame(badRun, new TGLayoutHints(kLHintsTop | kLHintsCenterX | kLHintsExpandX, 2, 2, 2, 2) );

   cframe2->AddFrame(shiftframe, new TGLayoutHints( kLHintsCenterY | kLHintsRight, 0, 0, 0, 0) );


   AddFrame(cframe2, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX | kLHintsExpandY, 2, 2, 5, 1));


   /////////////////////////////////
   /////      Bottom Row
   /////////////////////////////////
   // three buttons are resized up to the parent width.
   // Note! this width should be fixed!
   TGCompositeFrame *cframe3 = new TGCompositeFrame(this, 550, 20,
                                             kHorizontalFrame | kFixedWidth);

   writeBtn = new TGTextButton(cframe3, "Submit entry");
   writeBtn->Connect("Clicked()", "MyMainFrame", this, "endRun()");
   // to share whole parent space we need to use kLHintsExpandX layout hints
   cframe3->AddFrame(writeBtn, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
                                           3, 2, 2, 2));


   exit = new TGTextButton(cframe3, "&Exit ","gApplication->Terminate(0)");
   cframe3->AddFrame(exit, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
                                             2, 0, 2, 2));

   //Run Number
   cframe3->AddFrame(new TGLabel(cframe3, new TGString("Run#:")), new TGLayoutHints(kLHintsCenterY | kLHintsExpandX,2, 2, 2, 2) );
   RunNum= new TGNumberEntry(cframe3, 1, 12);
   RunNum->SetFormat( TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive );
   RunNum->SetLimitValues( 1, 10 );
   cframe3->AddFrame(RunNum, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   AddFrame(cframe3, new TGLayoutHints(kLHintsCenterX, 2, 2, 5, 1));

   /////////////////////////////////
   /////      Status Bar
   /////////////////////////////////

   StatusBar = new TGStatusBar(this, 50, 10, kVerticalFrame);
   StatusBar->Draw3DCorner(kFALSE);
   AddFrame(StatusBar, new TGLayoutHints(kLHintsExpandX, 0, 0, 10, 0));

   SetWindowName("CONFIG");

   // gives min/max window size + a step of x,y incrementing
   // between the given sizes
   SetWMSizeHints(500, 100, 1920, 1080, 1, 1);
   MapSubwindows();
   // important for layout algorithm
   Resize(GetDefaultSize());
   MapWindow();

  //  setDebugValues();
}


MyMainFrame::~MyMainFrame()
{
   // Clean up all widgets, frames and layouthints that were used
   Cleanup();
}


void MyMainFrame::WriteXML( Config cfg ){

    T4XMLWriter *myWriter = new T4XMLWriter();
    myWriter->addNode("Alignment");
    myWriter->createDataNode("Alignment",0,"Shift_leader", cfg.shiftLeader );
    myWriter->createDataNode("Alignment",0,"run"         , cfg.run );
    myWriter->createDataNode("Alignment",0,"phase"       , cfg.phase );
    myWriter->createDataNode("Alignment",0,"good_run"    , cfg.good );
    myWriter->createDataNode("Alignment",0,"x_table"     , cfg.x_det_table );
    myWriter->createDataNode("Alignment",0,"y_table"     , cfg.y_det_table );
    myWriter->createDataNode("Alignment",0,"x_trigger"   , cfg.x_trig_table );
    myWriter->createDataNode("Alignment",0,"y_trigger"   , cfg.y_trig_table );
    myWriter->createDataNode("Alignment",0,"beam_energy" , cfg.beam_energy );
    myWriter->createDataNode("Alignment",0,"beam_type"   , cfg.beam_type );
    myWriter->createDataNode("Alignment",0,"Detector"    , cfg.detector );
    myWriter->createDataNode("Alignment",0,"ZDC_HV"      , cfg.ZDC_HV );
    myWriter->createDataNode("Alignment",0,"ZDC_b1"      , cfg.ZDC_b1 );
    myWriter->createDataNode("Alignment",0,"ZDC_b2"      , cfg.ZDC_b2 );
    myWriter->createDataNode("Alignment",0,"ZDC_b3"      , cfg.ZDC_b3 );
    myWriter->createDataNode("Alignment",0,"RPD_HV1"     , cfg.RPD_HV1 );
    myWriter->createDataNode("Alignment",0,"RPD_HV2"     , cfg.RPD_HV2 );
    myWriter->createDataNode("Alignment",0,"RPD_HV3"     , cfg.RPD_HV3 );
    myWriter->createDataNode("Alignment",0,"RPD_HV4"     , cfg.RPD_HV4 );
    myWriter->createDataNode("Alignment",0,"EM_Proto_HV" , cfg.EM_HV );

    myWriter->createFile( Form("run%d.xml", cfg.run) );
    delete myWriter;
}

Config MyMainFrame::getCurrentConfig( int runNo ){
    Config cfg;

    cfg.shiftLeader  = (string)m_shiftLeaders[Shifter->GetSelected()-1];
    cfg.run          =         runNo;
    cfg.phase        =         Phase         ->GetNumber();
    cfg.good         =        (badRun        ->IsOn()) ? false : true;
    cfg.x_det_table  =         NumTblX       ->GetNumber();
    cfg.y_det_table  =         NumTblY       ->GetNumber();
    cfg.x_trig_table =         NumTrgX       ->GetNumber();
    cfg.y_trig_table =         NumTrgY       ->GetNumber();
    cfg.beam_energy  =         NumBeamE      ->GetNumber();
    cfg.beam_type    =        (Beam          ->GetSelected() == 1) ? (string)"electron" : (string)"proton";
    cfg.detector     =         m_detTypes    [Det->GetSelected()];
    cfg.ZDC_HV       =         ZDChv         ->GetNumber();
    cfg.ZDC_b1       =         ZDCb1         ->GetNumber();
    cfg.ZDC_b2       =         ZDCb2         ->GetNumber();
    cfg.ZDC_b3       =         ZDCb3         ->GetNumber();
    cfg.RPD_HV1      =         RPDhv1        ->GetNumber();
    cfg.RPD_HV2      =         RPDhv2        ->GetNumber();
    cfg.RPD_HV3      =         RPDhv3        ->GetNumber();
    cfg.RPD_HV4      =         RPDhv4        ->GetNumber();
    cfg.EM_HV        =         UEMhv         ->GetNumber();

    return cfg;
}



void MyMainFrame::openRootFile(TString fileName){
  TFile f(fileName.Data(), "read");
  if(!f.IsOpen()){
    errorMessage("Invalid File", "File does could not be opened");
    return;
  }

  TTree *t = (TTree*)f.Get("TB2023config");

  Config cfg;

  string *sl=0,*det=0,*beam=0;

  t->SetBranchAddress("shiftLeader",  &sl                   );
  t->SetBranchAddress("run",          &cfg.run              );
  t->SetBranchAddress("phase",        &cfg.phase            );
  t->SetBranchAddress("good",         &cfg.good             );
  t->SetBranchAddress("x_det_table",  &cfg.x_det_table      );
  t->SetBranchAddress("y_det_table",  &cfg.y_det_table      );
  t->SetBranchAddress("x_trig_table", &cfg.x_trig_table     );
  t->SetBranchAddress("y_trig_table", &cfg.y_trig_table     );
  t->SetBranchAddress("beam_energy",  &cfg.beam_energy      );
  t->SetBranchAddress("beam_type",    &beam                 );
  t->SetBranchAddress("detector",     &det                  );
  t->SetBranchAddress("ZDC_HV",       &cfg.ZDC_HV           );
  t->SetBranchAddress("ZDC_b1",       &cfg.ZDC_b1           );
  t->SetBranchAddress("ZDC_b2",       &cfg.ZDC_b2           );
  t->SetBranchAddress("ZDC_b3",       &cfg.ZDC_b3           );
  t->SetBranchAddress("RPD_HV1",      &cfg.RPD_HV1          );
  t->SetBranchAddress("RPD_HV2",      &cfg.RPD_HV2          );
  t->SetBranchAddress("RPD_HV3",      &cfg.RPD_HV3          );
  t->SetBranchAddress("RPD_HV4",      &cfg.RPD_HV4          );
  t->SetBranchAddress("EM_HV",        &cfg.EM_HV            );

  t->GetEntry(t->GetEntries()-1);

  cfg.shiftLeader = *sl;
  cfg.beam_type = *beam;
  cfg.detector = *det;

  fillFields(cfg);

  f.Close();

}

void MyMainFrame::createRootFile(Config cfg){

    cout << m_rootFileName << endl;
    TFile f(m_rootFileName.Data(), "recreate");
    if(!f.IsOpen()){
        errorMessage("Error creating file",Form("%s not found or is corrupted", m_rootFileName.Data() ) );
        return;
    }

    TTree *t = new TTree("TB2023config","TB2023config");

    t->Branch("shiftLeader",  &cfg.shiftLeader      );
    t->Branch("run",          &cfg.run              );
    t->Branch("phase",        &cfg.phase            );
    t->Branch("good",         &cfg.good             );
    t->Branch("x_det_table",  &cfg.x_det_table      );
    t->Branch("y_det_table",  &cfg.y_det_table      );
    t->Branch("x_trig_table", &cfg.x_trig_table     );
    t->Branch("y_trig_table", &cfg.y_trig_table     );
    t->Branch("beam_energy",  &cfg.beam_energy      );
    t->Branch("beam_type",    &cfg.beam_type        );
    t->Branch("detector",     &cfg.detector         );
    t->Branch("ZDC_HV",       &cfg.ZDC_HV           );
    t->Branch("ZDC_b1",       &cfg.ZDC_b1           );
    t->Branch("ZDC_b2",       &cfg.ZDC_b2           );
    t->Branch("ZDC_b3",       &cfg.ZDC_b3           );
    t->Branch("RPD_HV1",      &cfg.RPD_HV1          );
    t->Branch("RPD_HV2",      &cfg.RPD_HV2          );
    t->Branch("RPD_HV3",      &cfg.RPD_HV3          );
    t->Branch("RPD_HV4",      &cfg.RPD_HV4          );
    t->Branch("EM_HV",        &cfg.EM_HV            );

    t->Fill();
    // t->Write();
    f.Write();
    f.Close();
}

void MyMainFrame::updateRootFile(Config cfg){

    TFile f(m_rootFileName.Data(), "update");
  if(!f.IsOpen()){
    errorMessage("Error updating file",Form("%s not found or is corrupted", m_rootFileName.Data() ) );
    return;
  }

  TTree *t = (TTree*)f.Get("TB2023config");

  string *sl= new string(cfg.shiftLeader);
  string *det= new string(cfg.detector);
  string *beam= new string(cfg.beam_type);

  t->SetBranchAddress("shiftLeader",  &sl                   );
  t->SetBranchAddress("run",          &cfg.run              );
  t->SetBranchAddress("phase",        &cfg.phase            );
  t->SetBranchAddress("good",         &cfg.good             );
  t->SetBranchAddress("x_det_table",  &cfg.x_det_table      );
  t->SetBranchAddress("y_det_table",  &cfg.y_det_table      );
  t->SetBranchAddress("x_trig_table", &cfg.x_trig_table     );
  t->SetBranchAddress("y_trig_table", &cfg.y_trig_table     );
  t->SetBranchAddress("beam_energy",  &cfg.beam_energy      );
  t->SetBranchAddress("beam_type",    &beam                 );
  t->SetBranchAddress("detector",     &det                  );
  t->SetBranchAddress("ZDC_HV",       &cfg.ZDC_HV           );
  t->SetBranchAddress("ZDC_b1",       &cfg.ZDC_b1           );
  t->SetBranchAddress("ZDC_b2",       &cfg.ZDC_b2           );
  t->SetBranchAddress("ZDC_b3",       &cfg.ZDC_b3           );
  t->SetBranchAddress("RPD_HV1",      &cfg.RPD_HV1          );
  t->SetBranchAddress("RPD_HV2",      &cfg.RPD_HV2          );
  t->SetBranchAddress("RPD_HV3",      &cfg.RPD_HV3          );
  t->SetBranchAddress("RPD_HV4",      &cfg.RPD_HV4          );
  t->SetBranchAddress("EM_HV",        &cfg.EM_HV            );

  t->Fill();
  t->Write("", TObject::kOverwrite);
  // f.Write();
  f.Close();
}

bool MyMainFrame::validateIpAddress(const string &ipAddress){
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress.c_str(), &(sa.sin_addr));
    return result != 0;
}

void MyMainFrame::HandleMenu(Int_t id)
{
   // Handle menu items.
   const char *filetypes[] = { "All files",     "*",
                            "ROOT files",    "*.root",
                            "ROOT macros",   "*.C",
                            "Text files",    "*.[tT][xX][tT]",
                            0,               0 };

   switch (id) {

      case M_FILE_OPEN:
         {
            static TString dir(".");
            TGFileInfo fi;
            fi.fFileTypes = filetypes;
            fi.SetIniDir(dir);
            new TGFileDialog(gClient->GetRoot(), this, kFDOpen, &fi);

            m_rootFileName = fi.fFilename;
            openRootFile(m_rootFileName);
            m_rootFileOpen = true;
            
         }
         break;

      case M_FILE_SAVEAS:
          {
            static TString dir(".");
            TGFileInfo fi;
            fi.fFileTypes = filetypes;
            fi.SetIniDir(dir);
            new TGFileDialog(gClient->GetRoot(), this, kFDSave, &fi);

            m_rootFileName = fi.fFilename;
            m_rootFileOpen = false;
            dir = fi.fIniDir;
         }
         break;

      case M_FILE_EXIT:
         CloseWindow();   // terminate theApp no need to use SendCloseMessage()
         break;

      default:
         break;
   }
}

void MyMainFrame::setDebugValues(){
  Config cfg;

  cfg.shiftLeader = "Chad Lantz";
  cfg.run = 42;
  cfg.phase = 1;
  cfg.good = true;
  cfg.x_det_table = 750;
  cfg.y_det_table = 750;
  cfg.x_trig_table = 750;
  cfg.y_trig_table = 750;
  cfg.beam_energy = 150;
  cfg.beam_type = "electron";
  cfg.detector = "Arm 8-1";
  cfg.ZDC_HV = 1750;
  cfg.ZDC_b1 = 32;
  cfg.ZDC_b2 = 22;
  cfg.ZDC_b3 = 12;
  cfg.RPD_HV1 = 1250;
  cfg.RPD_HV2 = 1250;
  cfg.RPD_HV3 = 1250;
  cfg.RPD_HV4 = 1250;
  cfg.EM_HV = 1250;

  fillFields(cfg);

}

void MyMainFrame::fillFields(Config cfg){

    Shifter->Select( Shifter->FindEntry(cfg.shiftLeader.c_str())->EntryId() );
    Beam->Select( Beam->FindEntry(cfg.beam_type.c_str())->EntryId() );
    Det->Select( Det->FindEntry(cfg.detector.c_str())->EntryId() );

    RunNum->SetNumber(cfg.run);
    Phase->SetNumber(cfg.phase);
    if(cfg.good) badRun->SetState(kButtonDown);
    NumTblX->SetNumber(cfg.x_det_table);
    NumTblY->SetNumber(cfg.y_det_table);
    NumTrgX->SetNumber(cfg.x_trig_table);
    NumTrgY->SetNumber(cfg.y_trig_table);
    NumBeamE->SetNumber(cfg.beam_energy);
    ZDChv->SetNumber(cfg.ZDC_HV);
    ZDCb1->SetNumber(cfg.ZDC_b1);
    ZDCb2->SetNumber(cfg.ZDC_b2);
    ZDCb3->SetNumber(cfg.ZDC_b3);
    RPDhv1->SetNumber(cfg.RPD_HV1);
    RPDhv2->SetNumber(cfg.RPD_HV2);
    RPDhv3->SetNumber(cfg.RPD_HV3);
    RPDhv4->SetNumber(cfg.RPD_HV4);
    UEMhv->SetNumber(cfg.EM_HV);
}

/*
 * Perform consistency checks, write xml file, write elog entry, increment run counter and reset bad run box
 */
void MyMainFrame::endRun(){

  //Get runNo, nEvents and do consistency checks (see if data file is still open)
  if(Beam->GetSelected() == -1){
    errorMessage("No Beam Type selected", "Please select a beam type before adding an entry");
    return;
  }
  if(Det->GetSelected() < 1){
    errorMessage("No detector selected", "Please select a detector type before adding an entry");
  return;
  }
  if(Shifter->GetSelected() < 1){
    errorMessage("No shift leader selected", "Please select a shift leader before adding an entry");
    return;
  }

  //Get the table position from the pi
  if(detTableIP->GetBuffer()->GetTextLength() > 0){
    if(!validateIpAddress(detTableIP->GetBuffer()->GetString())){
      errorMessage("Invald IP address", "Enter a valid IP address for the detector table or empty the text box entirely and enter a value manually");
      return;
    }else{
      std::string tablePos = gSystem->GetFromPipe( Form("ssh pi@%s 'python3 H2_LaserDisto/get_pos.py'", detTableIP->GetBuffer()->GetString() ) ).Data();
      NumTblX->SetNumber( std::atof(tablePos.substr(tablePos.find_first_of(':') + 1, tablePos.find_first_of(',') - tablePos.find_first_of(':') ).c_str() ) );
      NumTblY->SetNumber( std::atof(tablePos.substr(tablePos.find_last_of(':') + 1, tablePos.length() ).c_str() ) );
    }
  }

  int runNo = (int)RunNum->GetNumber();

  Config cfg = getCurrentConfig( runNo );

  WriteXML(cfg);

  if(m_rootFileOpen){
    updateRootFile(cfg);
  }else{
    createRootFile(cfg);
    m_rootFileOpen = true;
  }

  gSystem->Exec( Form("elog -h localhost -p 8080 -l TB2023 -a Author=\"%s\" -a  Type=Software -a Category=Runs -a Subject=Partition_End_Run \"Run %d ended. \n\n %s\" -f run%d.xml",  m_shiftLeaders[Shifter->GetSelected()-1].Data(), runNo,  msgBox->GetText()->AsString().Data(), runNo ) );

  //Reset the bad run button
  if(badRun->IsOn()) badRun->SetState( kButtonUp );

  RunNum->SetNumber(runNo + 1);
}

void  MyMainFrame::errorMessage(TString _title, TString _message){
  Disconnect("CloseWindow()");

  new TGMsgBox(gClient->GetRoot(), this, _title.Data(), _message.Data(), kMBIconExclamation, kMBOk);

  //Reconnect the close window button once the error is acknowledged
  Connect("CloseWindow()", "MyMainFrame", this, "CloseWindow()");

}


void  MyMainFrame::ZDChvSet(){

  ZDCb1->SetNumber( ZDChv->GetNumber()*.367 );
  ZDCb2->SetNumber( ZDChv->GetNumber()*.256 );
  ZDCb3->SetNumber( ZDChv->GetNumber()*.112 );


  //Calculate the booster voltages and set the TGString

}
