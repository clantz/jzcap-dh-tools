//Note: TVector3 division by constant confuses the compiler which tries to convert the result to TQuaternion
//To avoid this, the vectors are simply multiplied by the inverse
//Big thanks to ChatGPT for making this possible


#include <TVector3.h>
#include <iostream>
#include <cmath>
#include <vector>

//The survey coordinate system is rotated with respect to the beam. This function rotates from the survey axis to beam axis
void RotateToBeamLineCoords(vector<TVector3*> &vIn);

//From a given set of vectors that approximate an orthonormal basis, find the nearest true orthonormal basis
void findNearestOrthonormalBasis(const TVector3& v1, const TVector3& v2, const TVector3& v3, TVector3& b1, TVector3& b2, TVector3& b3); //ChatGPT

//From a given set of vectors that describe an orthorormal basis, return a TQuaternion describing the rotation to that frame
TQuaternion basisVectorsToQuaternion(const TVector3& forward, const TVector3& up, const TVector3& right); //ChatGPT

//Given a ray, a plane normal and a point which lies on the plane described by the normal, find the point where the ray intersects with the plane
TVector3 findRayPlaneIntersection(const TVector3& rayOrigin, const TVector3& rayDirection,const TVector3& planeNormal, const TVector3& planePoint); //ChatGPT

//Get the projection of a given vector on a plane described by its normal vector
TVector3 GetProjection(TVector3 vIn, TVector3 vNormal);

//Given a set of vectors that lie on a plane, return the normal of that plane formed by all permutations the cross product of the vectors. 
//The cross product will be performed so the given/preferred component ("x", "y", or "z") is positive
TVector3 GetAverageNormal(vector<TVector3> vIn, TString axis);

//Custom functions for each survey
void doSurvey1(ofstream &file);
void doSurvey2(ofstream &file);
void DoZDCSurvey1(vector<TVector3*> vIn, vector<TVector3> &pos, TQuaternion &dir);
void DoZDCSurvey2(vector<TVector3*> vIn, vector<TVector3> &pos, TQuaternion &dir);
void DoProtoEM(vector<TVector3*> vIn, TVector3 &pos, TQuaternion &dir);
void DoRPD(vector<TVector3*> vIn, TVector3 &pos, TQuaternion &dir, TString side );

//Logisitcs
void addXMLentry(ofstream &file, TString name, int start, int end, TVector3 tablePos, TVector3 pos, TQuaternion dir);

//Establish dimensions of the ZDC
float ZDC_HEIGHT = 0.621; //m
float ZDC_BOTTOM_TO_CENTER_ACTIVE = 0.09121; //m
float ZDC_HALF_WIDTH =  0.046; //m
float ZDC_HALF_DEPTH =  0.077; //m
float ZDC_HAD1_PIXEL_DEPTH = 0.0322; //m
float GAP = 0.0005; //m

//Important RPD dimenisions
float RPD_BOTTOM_TO_CENTER_ACTIVE = 0.081; //m
float RPD_HALF_DEPTH =  0.015; //m
float RPD_HALF_WIDTH = 0.035; //m

//Important prototype EM dimensions
float PROTO_EM_BOTTOM_TO_CENTER_ACTIVE = 0.0768; //m
float PROTO_EM_HALF_DEPTH = 0.8055; //m
float PROTO_EM_HALF_WIDTH_TOP = 0.0445; //m
float PROTO_EM_HALF_WIDTH_BOTTOM = 0.023; //m


//Main function
void surveyMaker2023(){


  ofstream file("Survey_2023.xml");

  file << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>" << endl;
  file << "<root>" << endl;

  doSurvey1(file);
  doSurvey2(file);

  file << "</root>" << endl;
}


void doSurvey1(ofstream &file){

  int startRun = 95540;
  int endRun   = 95818;

  TVector3 tablePos(1.545,	.877, 0);
  TVector3 trigTablePos(.199,	.302, 0);

  vector<TVector3*> TRIGs;
  TRIGs.push_back( new TVector3( -11.8349, -0.0041, -0.0040) ); //TRIGGER_U_Centre
  TRIGs.push_back( new TVector3( -12.0275, 0.0003, -0.0001) ); //TRIGGER_D_Centre

  RotateToBeamLineCoords(TRIGs);

  addXMLentry(file, "TRIGGER", startRun, endRun, tablePos, (*TRIGs[0] + *TRIGs[1])*0.5, TQuaternion(0,0,0,0) ); //The table position and orientation aren't important here
  

  ///////////////////////////////////////////////////
  //        ARM 1-2
  ///////////////////////////////////////////////////

  // MEASURED POINTS ON ZDC
  vector<TVector3*> ZDC_12;
  ZDC_12.push_back( new TVector3(-10.0218, -0.0476, -0.0530) ); //ZDC_L_RUB
  ZDC_12.push_back( new TVector3(-10.0206, -0.0530,  0.3802) ); //ZDC_L_RUT
  ZDC_12.push_back( new TVector3(-9.5295 , -0.0475, -0.0650) ); //ZDC_L_RDB
  ZDC_12.push_back( new TVector3(-9.5300 , -0.0514,  0.5373) ); //ZDC_L_RDT
  ZDC_12.push_back( new TVector3(-9.9938 , -0.0551,  0.5548) ); //ZDC_L_T01, HAD1
  ZDC_12.push_back( new TVector3(-9.8139 , -0.0497,  0.5480) ); //ZDC_L_T02, HAD2
  ZDC_12.push_back( new TVector3(-9.6566 , -0.0505,  0.5476) ); //ZDC_L_T03, HAD3
  ZDC_12.push_back( new TVector3(-9.5285 ,  0.0449, -0.0637) ); //ZDC_L_LDB
  ZDC_12.push_back( new TVector3(-9.5289 ,  0.0419,  0.5368) ); //ZDC_L_LDT
  ZDC_12.push_back( new TVector3(-9.4667 , -0.0042, -0.0716) ); //ZDC_L_DH

  

  vector<TVector3> ZDC_12_POS;
  TQuaternion ZDC_12_DIR;
  DoZDCSurvey1(ZDC_12, ZDC_12_POS, ZDC_12_DIR);
  vector<TString> ZDC_12_NAMES = {"1-2HAD3", "1-2HAD2", "1-2HAD1"};

  for(int i = 0; i < 3; i++){
    addXMLentry(file, ZDC_12_NAMES[i], startRun, endRun, tablePos, ZDC_12_POS[i], ZDC_12_DIR);
  }

  vector<TVector3*> RPD_12;
  RPD_12.push_back( new TVector3( -10.0425, -0.0405,  0.1701) ); //RPD_L_RDB
  RPD_12.push_back( new TVector3( -10.0426, -0.0423,  0.3618) ); //RPD_L_RDT
  RPD_12.push_back( new TVector3( -10.0758, -0.0407,  0.1743) ); //RPD_L_RUB
  RPD_12.push_back( new TVector3( -10.0743, -0.0422,  0.3537) ); //RPD_L_RUT
  RPD_12.push_back( new TVector3( -10.0348, -0.0489, -0.0632) ); //RPD_L_DH


  TVector3 RPD_12_POS;
  TQuaternion RPD_12_DIR;
  DoRPD(RPD_12, RPD_12_POS, RPD_12_DIR,"1-2");

  addXMLentry(file, "1-2RPD", startRun, endRun, tablePos, RPD_12_POS, RPD_12_DIR);


  int emSwap1runNum = 95655; //Moved from 1-2 to 8-1
  int emSwap2runNum = 95732; //Moved from 8-1 to 1-2

  //One point was taken on the EM to give a longitudinal offset between the RPD and prototype EM
  TVector3 emCorner(-10.0972, -0.0250, -0.0529); //EM_L_DRB
  emCorner.SetXYZ(emCorner.y(),emCorner.z(),emCorner.x()); //Rotate to beamline coords

  //Give the prototype EM the same orientation as the RPD
  TVector3 RPD_12_TRANS = RPD_12_DIR.Rotation( TVector3(1,0,0) ); //Transverse direction
  TVector3 RPD_12_VERT  = RPD_12_DIR.Rotation( TVector3(0,1,0) ); //Vertical direction
  TVector3 RPD_12_LONG  = RPD_12_DIR.Rotation( TVector3(0,0,1) ); //Longitudinal direction


  TVector3 emBottomInsideDownstream = findRayPlaneIntersection( emCorner, RPD_12_VERT, RPD_12_VERT, *RPD_12[4]);
  TVector3 PROTO_EM_12_POS = emBottomInsideDownstream + PROTO_EM_BOTTOM_TO_CENTER_ACTIVE*RPD_12_VERT + PROTO_EM_HALF_WIDTH_BOTTOM*RPD_12_TRANS - PROTO_EM_HALF_DEPTH*RPD_12_LONG;

  addXMLentry(file, "PROTO_EM", startRun, emSwap1runNum-1, tablePos, PROTO_EM_12_POS, RPD_12_DIR);
  addXMLentry(file, "PROTO_EM", emSwap2runNum, endRun, tablePos, PROTO_EM_12_POS, RPD_12_DIR);

  ///////////////////////////////////////////////////
  //        ARM 8-1
  ///////////////////////////////////////////////////

  
  // MEASURED POINTS ON ZDC
  vector<TVector3*> ZDC_81;
  ZDC_81.push_back( new TVector3(-9.9615, -0.4990, -0.0508) ); //ZDC_R_LUB
  ZDC_81.push_back( new TVector3(-9.9615, -0.5017,  0.3822) ); //ZDC_R_LUT
  ZDC_81.push_back( new TVector3(-9.4693, -0.4998, -0.0628) ); //ZDC_R_LDB
  ZDC_81.push_back( new TVector3(-9.4671, -0.5019,  0.5378) ); //ZDC_R_LDT
  ZDC_81.push_back( new TVector3(-9.9435, -0.5035,  0.5566) ); //ZDC_R_T01
  ZDC_81.push_back( new TVector3(-9.7569, -0.5021,  0.5500) ); //ZDC_R_T02
  ZDC_81.push_back( new TVector3(-9.5950, -0.5024,  0.5498) ); //ZDC_R_T03
  ZDC_81.push_back( new TVector3(-9.4680, -0.5931, -0.0634) ); //ZDC_R_RDB
  ZDC_81.push_back( new TVector3(-9.4668, -0.5956,  0.5375) ); //ZDC_R_RDT
  ZDC_81.push_back( new TVector3(-9.4156, -0.5520, -0.0724) ); //ZDC_R_DH

  vector<TVector3> ZDC_81_POS;
  TQuaternion ZDC_81_DIR;
  DoZDCSurvey1(ZDC_81, ZDC_81_POS, ZDC_81_DIR);
  vector<TString> ZDC_81_NAMES = {"8-1HAD3", "8-1HAD2", "8-1HAD1"};


  //We moved the table to center arm 8-1 on the beam with Benoit (survey guy) there
  //to guide us to true center in x.
  //This section is to find the 
  vector<TVector3*> ZDC_MOVED_TABLE;
  ZDC_MOVED_TABLE.push_back( new TVector3( -9.4678, 0.0449, -0.0621) ) ; //ZDC_R_LDB02
  ZDC_MOVED_TABLE.push_back( new TVector3( -9.4671, -0.0482, -0.0627) ); //ZDC_R_RDB02
  RotateToBeamLineCoords(ZDC_MOVED_TABLE);

  //Find the offset in the table position from its true position when 8-1 was centered on the beam
  TVector3 tablePos81(1.007, .855, 0);
  float trueTableMove = (ZDC_MOVED_TABLE[0]->x() + ZDC_MOVED_TABLE[1]->x())*0.5 - ZDC_81_POS[0].x() ;
  float tableChange = tablePos.x() - tablePos81.x();
  float tableOffset = fabs(trueTableMove) - fabs(tableChange);

  cout << "tableOffset = " << tableOffset << endl;

  for(int i = 0; i < 3; i++){
    //Offset the detector position to account for the table error, then write to file
    ZDC_81_POS[i].SetX(ZDC_81_POS[i].x() + tableOffset);
    addXMLentry(file, ZDC_81_NAMES[i], startRun, endRun, tablePos, ZDC_81_POS[i], ZDC_81_DIR);
  }

  vector<TVector3*> RPD_81;
  RPD_81.push_back( new TVector3( -9.9828 , -0.5126,  0.1678) ); //RPD_R_LDB
  RPD_81.push_back( new TVector3( -9.9830 , -0.5141,  0.3580) ); //RPD_R_LDT
  RPD_81.push_back( new TVector3( -10.0152, -0.5122,  0.1484) ); //RPD_R_LUB
  RPD_81.push_back( new TVector3( -10.0143, -0.5141,  0.3632) ); //RPD_R_LUT
  RPD_81.push_back( new TVector3( -9.9741 , -0.5010, -0.0613) ); //RPD_R_DH

  
  TVector3 RPD_81_POS;
  TQuaternion RPD_81_DIR;
  DoRPD(RPD_81, RPD_81_POS, RPD_81_DIR,"8-1");



  //Offset the detector position to account for the table error, then write to file
  RPD_81_POS.SetX(RPD_81_POS.x() + tableOffset);
  addXMLentry(file, "8-1RPD", startRun, endRun, tablePos, RPD_81_POS, RPD_81_DIR);

  //offset the em from this RPD by the same as the offset of 1-2
  TVector3 emRPDoffset = PROTO_EM_12_POS - RPD_12_POS;
  TVector3 PROTO_EM_81_POS = RPD_81_POS + emRPDoffset;

  addXMLentry(file, "PROTO_EM", emSwap1runNum, emSwap2runNum-1, tablePos, PROTO_EM_81_POS, RPD_81_DIR);


}


void doSurvey2(ofstream &file){

  int startRun = 95819;
  int endRun   = 95975;

  TVector3 tablePos(1.001,	.857, 0);
  TVector3 trigTablePos(.199,	.302, 0);

  vector<TVector3*> TRIGs;
  TRIGs.push_back( new TVector3(-11.9874,  0.0056,  0.0069) ); //TRIGGER_U_Centre
  TRIGs.push_back( new TVector3(-11.8748,  0.0013,  0.0030) ); //TRIGGER_D_Centre

  RotateToBeamLineCoords(TRIGs);

  addXMLentry(file, "TRIGGER", startRun, endRun, tablePos, (*TRIGs[0] + *TRIGs[1])*0.5, TQuaternion(0,0,0,0) ); //The table position and orientation aren't important here

  ///////////////////////////////////////////////////
  //        ARM 1-2
  ///////////////////////////////////////////////////


  // MEASURED POINTS ON ZDC
  vector<TVector3*> ZDC_12;
  ZDC_12.push_back( new TVector3(-10.0218,  .4952 , -0.0305) ); //ZDC_L_RUB
  ZDC_12.push_back( new TVector3(-10.0207,  .4903 ,  0.3601) ); //ZDC_L_RUT
  ZDC_12.push_back( new TVector3(-9.5297 ,  .4961 , -0.0747) ); //ZDC_L_RDB
  ZDC_12.push_back( new TVector3(-9.5297 ,  .4923 ,  0.5168) ); //ZDC_L_RDT
  ZDC_12.push_back( new TVector3(-9.9859 ,  .4882 ,  0.5342) ); //ZDC_L_T01
  ZDC_12.push_back( new TVector3(-9.8181 ,  .4934 ,  0.5277) ); //ZDC_L_T02
  ZDC_12.push_back( new TVector3(-9.6587 ,  .4930 ,  0.5276) ); //ZDC_L_T03
  ZDC_12.push_back( new TVector3(-9.5283 ,  .5886 , -0.0837) ); //ZDC_L_LDB
  ZDC_12.push_back( new TVector3(-9.5288 ,  .5857 ,  0.5188) ); //ZDC_L_LDT
  ZDC_12.push_back( new TVector3(-9.4665 ,  .5412 , -0.0915) ); //ZDC_L_DH
  ZDC_12.push_back( new TVector3(-10.4014,  0.4903,  0.2803) ); //EM_L_RDB
  ZDC_12.push_back( new TVector3(-10.4024,  0.4875,  0.5243) ); //EM_L_RDT
  ZDC_12.push_back( new TVector3(-10.1468,  0.4937, -0.0840) ); //EM_L_RUB
  ZDC_12.push_back( new TVector3(-10.1482,  0.4875,  0.5043) ); //EM_L_RUT

  

  vector<TVector3> ZDC_12_POS;
  TQuaternion ZDC_12_DIR;
  DoZDCSurvey1(ZDC_12, ZDC_12_POS, ZDC_12_DIR);
  vector<TString> ZDC_12_NAMES = {"1-2HAD3", "1-2HAD2", "1-2HAD1", "1-2EM"};

  for(int i = 0; i < 4; i++){
    addXMLentry(file, ZDC_12_NAMES[i], startRun, endRun, tablePos, ZDC_12_POS[i], ZDC_12_DIR);
  }

  vector<TVector3*> RPD_12;
  RPD_12.push_back( new TVector3(-9.9827 ,  0.0319,  0.1499) ); //RPD_R_LDB
  RPD_12.push_back( new TVector3(-9.9819 ,  0.0296,  0.3615) ); //RPD_R_LDT
  RPD_12.push_back( new TVector3(-10.0150,  0.0319,  0.1555) ); //RPD_R_LUB
  RPD_12.push_back( new TVector3(-10.0150,  0.0299,  0.3597) ); //RPD_R_LUT
  RPD_12.push_back( new TVector3(-9.9738 ,  0.0448, -0.0813) ); //RPD_R_DH


  TVector3 RPD_12_POS;
  TQuaternion RPD_12_DIR;
  DoRPD(RPD_12, RPD_12_POS, RPD_12_DIR,"1-2");

  addXMLentry(file, "1-2RPD", startRun, endRun, tablePos, RPD_12_POS, RPD_12_DIR);

  ///////////////////////////////////////////////////
  //        ARM 8-1
  ///////////////////////////////////////////////////

  // MEASURED POINTS ON ZDC
  vector<TVector3*> ZDC_81;
  ZDC_81.push_back( new TVector3(-9.9619 ,  .0443 , -0.0714) ); //ZDC_R_LUB
  ZDC_81.push_back( new TVector3(-9.9618 ,  .0411 ,  0.3621) ); //ZDC_R_LUT
  ZDC_81.push_back( new TVector3(-9.4687 ,  .0433 , -0.0503) ); //ZDC_R_LDB
  ZDC_81.push_back( new TVector3(-9.4671 ,  .0421 ,  0.5170) ); //ZDC_R_LDT
  ZDC_81.push_back( new TVector3(-9.9276 ,  0.0392,  0.5361) ); //ZDC_R_T01
  ZDC_81.push_back( new TVector3(-9.7599 ,  0.0417,  0.5297) ); //ZDC_R_T02
  ZDC_81.push_back( new TVector3(-9.6010 ,  0.0412,  0.5298) ); //ZDC_R_T03
  ZDC_81.push_back( new TVector3(-9.4679 ,  0.0489, -0.0702) ); //ZDC_R_RDB
  ZDC_81.push_back( new TVector3(-9.4665 ,  0.0516,  0.5172) ); //ZDC_R_RDT
  ZDC_81.push_back( new TVector3(-9.4150 ,  0.0048, -0.0924) ); //ZDC_R_DH
  ZDC_81.push_back( new TVector3(-10.0801,  0.0418, -0.0819) ); //EM_R_LDB
  ZDC_81.push_back( new TVector3(-10.0810,  0.0372,  0.4762) ); //EM_R_LDT
  ZDC_81.push_back( new TVector3(-10.2374,  0.0373,  0.3096) ); //EM_R_LUB
  ZDC_81.push_back( new TVector3(-10.2384,  0.0362,  0.4900) ); //EM_R_LUT



  //We moved the table to center arm 8-1 on the beam with Benoit (survey guy) there
  //to guide us to true center in x.
  //This section is to find the 
  vector<TVector3*> ZDC_MOVED_TABLE;
  ZDC_MOVED_TABLE.push_back( new TVector3( -9.4678, 0.0449, -0.0621) ) ; //ZDC_R_LDB02
  ZDC_MOVED_TABLE.push_back( new TVector3( -9.4671, -0.0482, -0.0627) ); //ZDC_R_RDB02
  RotateToBeamLineCoords(ZDC_MOVED_TABLE);

  vector<TVector3> ZDC_81_POS;
  TQuaternion ZDC_81_DIR;
  DoZDCSurvey1(ZDC_81, ZDC_81_POS, ZDC_81_DIR);
  vector<TString> ZDC_81_NAMES = {"8-1HAD3", "8-1HAD2", "8-1HAD1", "8-1EM"};

  //Find the offset in the table position from its true position when 8-1 was centered on the beam
  TVector3 tablePos81(1.007, .855, 0);
  float trueTableMove = (ZDC_MOVED_TABLE[0]->x() + ZDC_MOVED_TABLE[1]->x())/2.0 - ZDC_81_POS[0].x() ;
  float tableChange = tablePos.x() - tablePos81.x();
  float tableOffset = fabs(trueTableMove) - fabs(tableChange);

  for(int i = 0; i < 4; i++){
    //Offset the detector position to account for the table error, then write to file
    ZDC_81_POS[i].SetX(ZDC_81_POS[i].x() - tableOffset);
    addXMLentry(file, ZDC_81_NAMES[i], startRun, endRun, tablePos, ZDC_81_POS[i], ZDC_81_DIR);
  }

  vector<TVector3*> RPD_81;
  RPD_81.push_back( new TVector3(-10.0425,  0.5024,  0.1482) ); //RPD_L_RDB
  RPD_81.push_back( new TVector3(-10.0432,  0.5010,  0.3432) ); //RPD_L_RDT
  RPD_81.push_back( new TVector3(-10.0756,  0.5022,  0.1551) ); //RPD_L_RUB
  RPD_81.push_back( new TVector3(-10.0743,  0.5010,  0.3380) ); //RPD_L_RUT
  RPD_81.push_back( new TVector3(-10.0316,  0.4938, -0.0831) ); //RPD_L_DH
  
  TVector3 RPD_81_POS;
  TQuaternion RPD_81_DIR;
  DoRPD(RPD_81, RPD_81_POS, RPD_81_DIR,"8-1");
  addXMLentry(file, "RPD", startRun, endRun, tablePos, RPD_81_POS, RPD_81_DIR);


  ///////////////////////////////////////////////////
  //        Prototype EM
  ///////////////////////////////////////////////////

  vector<TVector3*> PROTO_EM;
  PROTO_EM.push_back( new TVector3(-10.1849,  0.1665,  0.4252) ); //EM_P_DLT
  PROTO_EM.push_back( new TVector3(-10.1848,  0.1675,  0.3149) ); //EM_P_DLB
  PROTO_EM.push_back( new TVector3(-10.1853,  0.1015,  0.1451) ); //EM_P_DRT
  PROTO_EM.push_back( new TVector3(-10.1852,  0.1028,  0.0133) ); //EM_P_DRB
  PROTO_EM.push_back( new TVector3(-10.1841,  0.1623, -0.0869) ); //EM_P_H


  TVector3 PROTO_EM_POS;
  TQuaternion PROTO_EM_DIR;
  DoProtoEM(PROTO_EM, PROTO_EM_POS, PROTO_EM_DIR);
  addXMLentry(file, "PROTO_EM", startRun, endRun, tablePos, PROTO_EM_POS, PROTO_EM_DIR);

}



//Parse the survey points to get the position of the center of the active volume for each ZDC on a given side
//as setup in survey 2 of the 2022 test beam
void DoZDCSurvey1(vector<TVector3*> vIn, vector<TVector3> &pos, TQuaternion &dir){

  RotateToBeamLineCoords(vIn);

  //Vector order is
  //[0]=Bottom Inner Upstream, [1]=Top Inner Upstream,       [2]=Bottom Inner Downstream, 
  //[3]=Top Inner Downstream,  [4]=Top HAD3,                 [5]=Top HAD2,                 
  //[6]=Top HAD1,              [7]=Bottom Outter Downstream, [8]=Top Outter Downstream, 
  //[9]=Top of rail system

  //Construct all the direction vectors in the survey plane
  vector< TVector3 > vSidePlane;
  for(int i = 1; i < 7; i++){
    for(int j = i+1; j <= 4; j++){
      vSidePlane.push_back( TVector3( *vIn[j] - *vIn[i] ) );
    }
  }
  //Get the transverse vector in the detector coords
  TVector3 vTrans = GetAverageNormal(vSidePlane,"x");

  vector< TVector3 > vBackPlanePoints = {*vIn[2], *vIn[3], *vIn[7], *vIn[8]};
  vector< TVector3 > vBackPlane;
  for(int i = 0; i < 4; i++){
    for(int j = i+1; j < 4; j++){
      vBackPlane.push_back( TVector3( vBackPlanePoints[j] - vBackPlanePoints[i] ) );
    }
  }

  //Get the longitudinal vector in the detector coords (Rear detector only)
  TVector3 vLong = GetAverageNormal(vBackPlane,"z");

  //Make the vertical vector in detector coordinates (Trusted detector vertical direction)
  TVector3 vVert = vLong.Cross(vTrans).Unit();

  //Get the average of the longitudinal components from the side face points (Average over all HADS) which likely has some vertical component
  TVector3 vLongAverage = (*vIn[2] - *vIn[0]) + (*vIn[3] - *vIn[1]);
  
  //Get the projection of the longitudinal vector on the horizontal plane
  vLongAverage = GetProjection(vLongAverage, vVert).Unit();
  
  TVector3 b1, b2, b3;
  findNearestOrthonormalBasis( vTrans, vVert, vLongAverage, b1, b2, b3);

  //Change to the new orthonormal basis
  vTrans = b1;
  vVert = b2;
  vLong = b3;

  //Average position of the back edge on the bottom only
  TVector3 backPos = *vIn[2] + *vIn[7];
  backPos *= 0.5;

  //Trace down the side of the detector to find the bottom
  TVector3 bottomRearCenter = findRayPlaneIntersection(backPos, vVert, vVert, *vIn[9]);

  //Get from the bottom rear corner of HAD3 to the center of the active volume
  TVector3 had3Center = bottomRearCenter + vVert*ZDC_BOTTOM_TO_CENTER_ACTIVE - vLong*ZDC_HALF_DEPTH;

  //Set the quaternion with the rotation matrix
  dir = basisVectorsToQuaternion(vLong, vVert, vTrans);
  
  pos.push_back(had3Center);//HAD3
  pos.push_back(had3Center - vLongAverage*(2*ZDC_HALF_DEPTH + GAP)); //HAD2
  pos.push_back(had3Center - vLongAverage*(4*ZDC_HALF_DEPTH + ZDC_HAD1_PIXEL_DEPTH + GAP) ); //HAD1

}


//Parse the survey ppints to get the position of the center of the active volume for an RPD
//as setup in survey 1 of the 2023 test beam
void DoRPD(vector<TVector3*> vIn, TVector3 &pos, TQuaternion &dir, TString side ){
  RotateToBeamLineCoords(vIn);

  //RPD_L_RDB, RPD_L_RDT, RPD_L_RUB, RPD_L_RUT, RPD_L_DH
  //Vector order is
  //[0]=Bottom Downstream, [1]=Top Downstream,     [2]=Bottom Upstream, 
  //[3]=Top Upstream,      [4]=Top of bottom of foot

  //Position between top two survey points
  TVector3 bottomCenter = (*vIn[0] + *vIn[2])*0.5;
  //Position between top two survey points
  TVector3 topCenter = (*vIn[1] + *vIn[3])*0.5;

  //Vertical center line of the front face
  TVector3 vVert = (topCenter - bottomCenter).Unit();  

  //Construct all the direction vectors in the survey plane
  vector< TVector3 > vPlane;
  for(int i = 1; i < 3; i++){
    for(int j = i+1; j <= 3; j++){
      vPlane.push_back( TVector3( *vIn[j] - *vIn[i] ) );
    }
  }

  //The normal of the front face of the RPD (facing downstream)
  TVector3 vTrans = GetAverageNormal(vPlane,"x");

  //Use our two vectors to get the third in our basis
  TVector3 vLong = vTrans.Cross(vVert);

  TVector3 b1, b2, b3;
  findNearestOrthonormalBasis( vTrans, vVert, vLong, b1, b2, b3);

  //Change to the new orthonormal basis
  vTrans = b1;
  vVert = b2;
  vLong = b3;

  //Location of the top of the bottom of the foot in the z center of the detector
  TVector3 bottomRef = findRayPlaneIntersection( bottomCenter, vVert, vVert, *vIn[4]);

  if(side == "1-2"){
    pos = bottomRef + vTrans*RPD_HALF_WIDTH + vVert*RPD_BOTTOM_TO_CENTER_ACTIVE;
  }else{
    pos = bottomRef - vTrans*RPD_HALF_WIDTH + vVert*RPD_BOTTOM_TO_CENTER_ACTIVE;
  }

  //Set the quaternion with the rotation matrix
  dir = basisVectorsToQuaternion(vLong, vVert, vTrans);
}


void DoZDCSurvey2(vector<TVector3*> vIn, vector<TVector3> &pos, TQuaternion &dir, TQuaternion &dirEM, TString side){
  //The first set of ZDC points are the same as in survey 1
  DoZDCSurvey1(vIn, pos, dir);


  //Vector order is
  //[0]=Bottom Inner Upstream,     [1]=Top Inner Upstream,          [2]=Bottom Inner Downstream, 
  //[3]=Top Inner Downstream,      [4]=Top HAD3,                    [5]=Top HAD2,                 
  //[6]=Top HAD1,                  [7]=Bottom Outter Downstream,    [8]=Top Outter Downstream, 
  //[9]=Top of rail system,        [10]=EM Bottom Inner Downstream, [11]EM Top Inner Downstream,
  //[12]=EM Bottom Inner Upstream, [13]=EM Top Inner Upstream

  //Use the direction of the quaternion for the HADS to define the horizontal plane of the EM (Assumes the 80/20 rails are reasonably straight)
  TVector3 vVert(0,1,0);
  dir.Rotate(vVert);

  //Construct all the direction vectors in the survey plane
  vector< TVector3 > vSidePlane;
  for(int i = 10; i < 13; i++){
    for(int j = i+1; j <= 13; j++){
      vSidePlane.push_back( TVector3( *vIn[j] - *vIn[i] ) );
    }
  }
  //Get the transverse vector in the detector coords
  TVector3 vTrans = GetAverageNormal(vSidePlane,"x");

  //The longitudinal component is retrieved by forming the two forward pointing vectors on the side face and projecting 
  //them onto the horizontal plane (described by its normal)
  TVector3 vLong = GetProjection( ( (*vIn[10] - *vIn[12]) + (*vIn[11] - *vIn[13]) ).Unit(), vVert);

  TVector3 b1, b2, b3;
  findNearestOrthonormalBasis( vTrans, vVert, vLong, b1, b2, b3);

  //Change to the new orthonormal basis
  vTrans = b1;
  vVert = b2;
  vLong = b3;

  TVector3 bottomInsideCenter = findRayPlaneIntersection(  (*vIn[10] + *vIn[12])*0.5, vVert, vVert, *vIn[9]);

  TVector3 activeCenterEM;
  if(side == "8-1"){
    pos.push_back( bottomInsideCenter + ZDC_BOTTOM_TO_CENTER_ACTIVE*vVert - ZDC_HALF_WIDTH*vTrans );
  }else{
    pos.push_back( bottomInsideCenter + ZDC_BOTTOM_TO_CENTER_ACTIVE*vVert + ZDC_HALF_WIDTH*vTrans );
  }

  dirEM = basisVectorsToQuaternion(vLong, vVert, vTrans);

}

void DoProtoEM(vector<TVector3*> vIn, TVector3 &pos, TQuaternion &dir){
  //Vector indecies are:
  //[0]Top Left,  [1]Bottom Left,
  //[2]Top Right, [3]Bottom Right, 
  //[4]Top of rail

  TVector3 vVert = ((*vIn[0] - *vIn[1]) + (*vIn[2] - *vIn[3])).Unit();

  //Construct all the direction vectors in the survey plane
  vector< TVector3 > vBackPlane;
  for(int i = 0; i < 3; i++){
    for(int j = i+1; j <= 3; j++){
      vBackPlane.push_back( TVector3( *vIn[j] - *vIn[i] ) );
    }
  }
  //Get the normal of the back plane of the detector
  TVector3 vLong = GetAverageNormal(vBackPlane, "z");

  //The last basis vector approximate: Y cross Z = X
  TVector3 vTrans = vVert.Cross(vLong);

  //Go from our approximate basis vectors to the nearest true orthonormal basis
  TVector3 b1, b2, b3;
  findNearestOrthonormalBasis( vTrans, vVert, vLong, b1, b2, b3);

  //Change to the new orthonormal basis
  vTrans = b1;
  vVert = b2;
  vLong = b3;

  //Find the bottom right corner of the detector
  TVector3 bottomRight = findRayPlaneIntersection(*vIn[3], vVert, vVert, *vIn[3]);

  //Move from the bottom right corner to the center of the active volume
  pos = bottomRight + vVert*PROTO_EM_BOTTOM_TO_CENTER_ACTIVE - vLong*PROTO_EM_HALF_DEPTH - vTrans*PROTO_EM_HALF_WIDTH_BOTTOM;

  //Get the quaternion that describes the detector's frame
  dir = basisVectorsToQuaternion(vLong, vVert, vTrans); 

}

//The survey coordinate system has x down the beamline axis
void RotateToBeamLineCoords(vector<TVector3*> &vIn){
  for(TVector3* vec: vIn){
    vec->SetXYZ(vec->y(),vec->z(),vec->x());
  }
}

//Take the average cross product of all given vectors such that the component in the specified axis is positive
TVector3 GetAverageNormal(vector<TVector3> vIn, TString axis){
  axis.ToLower();
  int nPoints = vIn.size();
  TVector3 normal(0,0,0);
  TVector3 buffer;
  for(int i = 0; i < nPoints; i++){
    for(int j = i+1; j < nPoints; j++){
      buffer = vIn[i].Cross(vIn[j]);
        if( (axis == "x" && buffer.x() < 0.0) ||
            (axis == "y" && buffer.y() < 0.0) ||
            (axis == "z" && buffer.z() < 0.0) ) buffer = vIn[j].Cross(vIn[i]);
      normal += buffer;
    }
  }
  return normal.Unit();
}


// Function to find the nearest orthonormal basis vectors
void findNearestOrthonormalBasis(const TVector3& v1, const TVector3& v2, const TVector3& v3, TVector3& b1, TVector3& b2, TVector3& b3) {
    b1 = v1.Unit();  // Normalize v1 to get the first basis vector b1

    // Gram-Schmidt process
    TVector3 projection_v2 = v2 - b1 * v2.Dot(b1);  // Projection of v2 onto the plane orthogonal to b1
    b2 = projection_v2.Unit();  // Normalize the projection to get the second basis vector b2

    // Calculate the third basis vector b3 using cross product
    TVector3 cross_product = b1.Cross(b2);
    b3 = cross_product.Unit();
}


//Gets the projection of a vector on a plane defined by a normal vector
TVector3 GetProjection(TVector3 vIn, TVector3 vNormal){
  
  TVector3 dot = vNormal*vIn.Dot(vNormal);
  dot *= 1/vNormal.Mag2(); //Do this otherwise the compiler confuses TVector3 for TQuaternion

  return vIn - dot;
}


// Function to find the intersection point of a ray and a plane
TVector3 findRayPlaneIntersection(const TVector3& rayOrigin, const TVector3& rayDirection,
                                  const TVector3& planeNormal, const TVector3& planePoint) {
    double denominator = rayDirection.Dot(planeNormal);
    
    // Check if the ray is parallel to the plane
    if (std::abs(denominator) < 1e-6) {
        std::cerr << "Error: Ray is parallel to the plane." << std::endl;
        return TVector3(0.0, 0.0, 0.0);
    }
    
    double t = (planePoint - rayOrigin).Dot(planeNormal) / denominator;
    return rayOrigin + t * rayDirection;
}


TQuaternion basisVectorsToQuaternion(const TVector3& forward, const TVector3& up, const TVector3& right) {
    // Calculate the components of the quaternion
    double trace = forward.x() + up.y() + right.z();
    

    float w=0,x=0,y=0,z=0;
    if (trace > 0) {
        double s = 0.5 / sqrt(trace + 1.0);
        w = 0.25 / s;
        x = (up.z() - right.y()) * s;
        y = (right.x() - forward.z()) * s;
        z = (forward.y() - up.x()) * s;
    } else if (forward.x() > up.y() && forward.x() > right.z()) {
        double s = 2.0 * sqrt(1.0 + forward.x() - up.y() - right.z());
        w = (up.z() - right.y()) / s;
        x = 0.25 * s;
        y = (up.x() + forward.y()) / s;
        z = (right.x() + forward.z()) / s;
    } else if (up.y() > right.z()) {
        double s = 2.0 * sqrt(1.0 + up.y() - forward.x() - right.z());
        w = (right.x() - forward.z()) / s;
        x = (up.x() + forward.y()) / s;
        y = 0.25 * s;
        z = (right.y() + up.z()) / s;
    } else {
        double s = 2.0 * sqrt(1.0 + right.z() - forward.x() - up.y());
        w = (forward.y() - up.x()) / s;
        x = (right.x() + forward.z()) / s;
        y = (right.y() + up.z()) / s;
        z = 0.25 * s;
    }


    return TQuaternion(w,x,y,z);
}


//Add a single XML entry
void addXMLentry(ofstream &file, TString name, int start, int end, TVector3 tablePos, TVector3 pos, TQuaternion dir){
  file <<      "    <Survey>"                                            << endl;
  file << Form("      <start_run>%d</start_run>", start               )  << endl;
  file << Form("      <end_run>%d</end_run>",     end                 )  << endl;
  file << Form("      <detector>%s</detector>",   name.Data()         )  << endl;
  file << Form("      <x_pos>%f</x_pos>",         pos.x()*1000        )  << endl;
  file << Form("      <y_pos>%f</y_pos>",         pos.y()*1000        )  << endl;
  file << Form("      <z_pos>%f</z_pos>",         pos.z()*1000        )  << endl;
  file << Form("      <i>%f</i>",                 dir.fVectorPart.x() )  << endl;
  file << Form("      <j>%f</j>",                 dir.fVectorPart.y() )  << endl;
  file << Form("      <k>%f</k>",                 dir.fVectorPart.z() )  << endl;
  file << Form("      <w>%f</w>",                 dir.fRealPart       )  << endl;
  file << Form("      <table_x>%f</table_x>",     tablePos.x()*1000   )  << endl;
  file << Form("      <table_y>%f</table_y>",     tablePos.y()*1000   )  << endl;
  file <<      "    </Survey>"                                           << endl;
}
