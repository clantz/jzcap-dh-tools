
/*
 * This is intended to be run in a root session
 *
 * Load the script
 * root [0] .L signalViewer.cpp
 *
 * Open a file. Defaults to LightGuideRun1459.root
 * root [1] openFile("nameOfFile")
 *
 * Print out a new canvas with the waveform for a particular "event"
 * root [2] plotOne(event)
 *
 * Or loop over all events for processing
 * root [2] Loop()
*/

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TObjArray.h>

#include "vector"

using namespace std;

// It's nice to make these global so we can avoid weird memory issues
// if a function forgets to close a file after use
TFile *f = new TFile();
TTree *tree;
// Draws a canvas with all channels for a given event
void openFile(string fileName = "LightGuideRun1459.root"){
  if(f->IsOpen()){ f->Close(); } // Close the file if you have one already open
  f = TFile::Open(fileName.c_str()); // Open the file
  f->GetObject("tree",tree);  // Retrieve the tree from the file
}


//Outputs a single waveform as a histogram
void plotOne(int ev, int channel = 0, int freq=500){
  if(!f->IsOpen()){
    cout << "Open the file first" << endl;
    return;
  }

  TObjArray *bNames = tree->GetListOfBranches();
  int nBranches = tree->GetNbranches();
  TString chStr = to_string(channel);
  TString name;
  //Check the list of branch names for the one you want
  for(int branch = 0; branch < nBranches; branch++){
    name = bNames->At(branch)->GetName();
    if(  name.Contains(chStr) ){
      break;
    }else if( branch == nBranches - 1){
      cout << "Couldn't find channel " << channel << " in this file" << endl;
      return;
    }
  }

  vector< float >* pvWF = 0;
  tree->SetBranchAddress( Form("RawSignal%d", channel), &pvWF );
  tree->GetEntry( ev );

  int nBin = pvWF->size();
  TH1D* h1 = new TH1D( Form( "Ev%d_RawSignal%d", ev, channel), Form( "Ev%d_RawSignal%d", ev, channel ), nBin, 0, nBin );

  for( uint bin = 0; bin < nBin; bin++ ){
      h1->SetBinContent( bin+1, pvWF->at(bin) );
  }

  TCanvas *c = new TCanvas( Form("Event%d", ev) , Form("Event%d", ev),800,600);
  h1->Draw();
}


//Outputs a single waveform as a histogram
void plotAll(Long64_t event){
  if(!f->IsOpen()){
    cout << "Open the file first" << endl;
    return;
  }

  //This needs to be set for some reason that is beyond me at the moment
  tree->SetMakeClass(1);

  vector< vector< float >* > signals;
  vector< TString > names;

  //Assign vectors to raw signal branches
  TObjArray *branches = tree->GetListOfBranches();
  int nCh = tree->GetNbranches();
  for(int ch = 0; ch < nCh; ch++){
    TString name = branches->At(ch)->GetName();
    if( !name.Contains("Raw") ) continue; //Only use branches with raw signals
    names.push_back( name );
    signals.push_back( 0 );
    tree->SetBranchAddress( name.Data(), &signals.back() );
  }

  nCh = signals.size();
  vector< TH1D* > h;

  //Fill histograms with the data
  tree->GetEntry( event );
  for(int ch = 0; ch < nCh; ch++){
    int nBins = signals[ch]->size();
    if( nBins <= 0 ) continue; //Channels that weren't used are empty
    h.push_back( new TH1D( names[ch].Data(), names[ch].Data(), nBins, 1, nBins ) );
    for( uint bin = 0; bin < nBins; bin++ ){
        h.back()->SetBinContent( bin+1, signals[ch]->at(bin));
    }
  }

  //Draw
  TCanvas *c = new TCanvas( Form("Event%lld", event) , Form("Event%lld", event),800,600);
  int pads = ceil(sqrt(h.size()));
  c->Divide(pads,pads);
  for( int pad = 0; pad < h.size(); pad++){
    c->cd(pad+1);
    h[pad]->Draw();
  }
}


/*
 * Plot a range of channels for a given event
*/
void plotRange(Long64_t event, int first, int last){
  if(!f->IsOpen()){
    cout << "Open the file first" << endl;
    return;
  }

  //This needs to be set for some reason that is beyond me at the moment
  tree->SetMakeClass(1);

  vector< vector< float >* > signals;
  vector< TString > names;
  vector< TH1D* > h;

  for(int ch = first; ch <= last; ch++){
    signals.push_back( 0 );
    names.push_back(Form("RawSignal%d",ch));
    tree->SetBranchAddress( names.back().Data(), &signals.back() );
  }

  //Fill histograms with the data
  tree->GetEntry( event );
  int nCh = last - first + 1;
  for(int ch = 0; ch < nCh; ch++){
    if(!signals[ch]) continue;
    int nBins = signals[ch]->size();
    if( nBins <= 0 ) continue; //Channels that weren't used are empty
    h.push_back( new TH1D( names[ch].Data(), names[ch].Data(), nBins, 1, nBins ) );
    for( uint bin = 0; bin < nBins; bin++ ){
        h.back()->SetBinContent( bin+1, signals[ch]->at(bin));
    }
  }

  //Draw
  TCanvas *c = new TCanvas( Form("Event%lld", event) , Form("Event%lld", event),1920,1080);
  int pads = ceil(sqrt(nCh));
  c->Divide(pads,pads);
  for( int pad = 0; pad < h.size(); pad++){
    c->cd(pad+1);
    h[pad]->Draw();
    h[pad]->SetAxisRange(2600,2950,"Y");
  }


}


void Loop(){
  if(!f->IsOpen()){
    cout << "Open the file first" << endl;
    return;
  }

  vector< float >* pvWF = new vector<float>(1024,0);
  tree->SetBranchAddress( "RawSignal0", &pvWF );

  // Make a histogram to store your processed data
  TH1D* h1 = new TH1D( "Charge", "Charge", 200, 0, 100 );

  // Create a canvas (window) to draw the histogram on
  TCanvas *c = new TCanvas( "Charge Spectrum" , "Charge Spectrum",800,600);

  // We loop over every event in the file and process each the same way
  int nEvents = tree->GetEntries(); // Get nEvents to avoid repeated function calls within the loop
  for(int ev = 0; ev < nEvents; ev++){
    double YourProcessedData = 0;

    tree->GetEntry( ev );

    //Do your processing here i.e.
    // YourProcessedData = someFunc(pvWF) or type it all here, either way works

    // Fill the histogram the proper way
    h1->Fill( YourProcessedData );

  }

  //h->Draw();

}
