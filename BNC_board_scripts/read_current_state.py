import time
import serial # pyserial
import sys, glob


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


print('scanning for COM ports')
ports = serial_ports()
print('Found ports: {}'.format(ports))

conn = None
no_connection = True

for p in ports:
    try:
        c = serial.Serial(port=p)
        c.write("*IDN?\r\n".encode())
        reply = c.readline()
        print(p + ': ' + str(reply))
        if reply != '9512+-0000-01.40\r\n'.encode():
            continue

        print('Found BNC board')
        conn = c
        no_connection = False
        break
    except Exception as e:
        print(p, e)

if no_connection:
    print('9512+-0000-01.40 not found in COM ports, exiting.')
    exit(1)

def write_to_BNC(message):

    conn.write((message + '?\r\n').encode())
    reply = str(conn.readline())

    return reply

commands = [
    ':PULSE1:STATE', 
    ':PULSE1:POL', 
    ':PULSE1:OUTPUT:MODE',
    ':PULSE1:OUTPUT:AMPL',
    ':PULSE1:WIDT', 
    ':PULSE1:DELAY', 

    ':PULSE2:STATE', 
    ':PULSE2:POL',
    ':PULSE2:OUTPUT:MODE',
    ':PULSE2:OUTPUT:AMPL',
    ':PULSE2:WIDT', 
    ':PULSE2:DELAY', 

    ':PULSE0:MODE', 
    ':SPULSE:EXTERNAL:MODE', 
    ':SPULSE:EXTERNAL:LEV', 
    ':SPULSE:EXTERNAL:EDGE', 
    ':PULSE0:STATE', 
    ':INST:STATE', 
]

for command in commands:
    time.sleep(0.2)
    reply = write_to_BNC(command).strip()

    print('{}: {}'.format(command, reply))
        
