import time
import serial # pyserial
import sys, glob


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


print('scanning for COM ports')
ports = serial_ports()
print('Found ports: {}'.format(ports))

conn = None
no_connection = True

for p in ports:
    try:
        c = serial.Serial(port=p, baudrate=9600, timeout=1)
        c.write("*IDN?\r\n".encode())
        reply = c.readline()
        print(p + ': ' + str(reply))
        if reply != '9512+-0000-01.40\r\n'.encode():
            continue

        print('Found BNC board')
        conn = c
        no_connection = False
        break
    except Exception as e:
        print(p, e)

if no_connection:
    print('9512+-0000-01.40 not found in COM ports, exiting.')
    exit(1)

def write_to_BNC(message):
    print('  --->\t{}'.format(message))

    conn.write((message + '\r\n').encode())
    reply = str(conn.readline())

    return reply

commands = [
    '*RST', 

    ':PULSE1:STATE ON', 
    ':PULSE1:POL NORM',
    ':PULSE1:OUTPUT:MODE ADJ',
    ':PULSE1:OUTPUT:AMPL 20',
    ':PULSE1:WIDT 0.000000009', 
    ':PULSE1:DELAY 0', 

    ':PULSE2:STATE ON', 
    ':PULSE2:POL NORM',
    ':PULSE2:OUTPUT:MODE ADJ',
    ':PULSE2:OUTPUT:AMPL 20',
    ':PULSE2:WIDT 0.000000009', 
    ':PULSE2:DELAY 0', 

    ':PULSE0:MODE SING', 
    ':SPULSE:EXTERNAL:MODE TRIG', 
    ':SPULSE:EXTERNAL:LEV 2.5', 
    ':SPULSE:EXTERNAL:EDGE RIS', 
    ':PULSE0:STATE ON', 
    ':INST:STATE ON', 

    '*SAV 1'
]

OK_REPLY = 'b\'ok\\r\\n\''

for command in commands:
    i = 0
    reply = ''
    while i < 5:
        time.sleep(0.2)
        reply = write_to_BNC(command).strip()

        if reply.find('ok') >= 0:
            break

        i += 1
        print('command \'{}\' got non ok reply \'{}\', trying again ({}/5)'.format(command, reply, i))
        
    if reply.find('ok') == -1:
        print('command \'{}\' failed after 5 tries, exiting'.format(command))
        exit(1)
        
