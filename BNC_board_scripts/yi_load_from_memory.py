import time
import serial # pyserial
import sys, os
import serial.tools.list_ports

# left, middle, right
USB_serial_numbers = ['AB0NHY3IA', 'AG0JLVNLA', 'AB0NIBA4A']#[:2]
IDN_serial_numbers = ['9512+-0000-01.40\r\n'.encode(), '9512+-0000-01.40\r\n'.encode(), '9512+-0000-01.40\r\n'.encode()]#[:2]
commands_list = [
    [
        '*RCL 1', 
        ':PULSE0:STATE ON', 
    ]
]*3

OK_REPLY = 'ok\r\n'.encode()
ports = [None, None, None]#[:2]
connections = [None, None, None]#[:2]

LINUX = None

path = os.path.realpath(__file__)
directory, _ = os.path.split(path)
log_directory = os.path.join(directory, "log")
try:
    os.mkdir(log_directory)
except FileExistsError: # directory already exists
    pass

log_file = open(os.path.join(log_directory, f'{time.strftime("%Y_%m_%d-%I_%M_%S_%p")}.txt'), 'w+')

def printb(string, end='\n'):
    print(string, end=end)
    log_file.write(string + end)
    log_file.flush()

if sys.platform.startswith('win'):
    LINUX = False
    printb('\nwindows mode')
elif sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
    LINUX = True
    printb('\nlinux mode')
else:
    raise EnvironmentError('Unsupported platform')
    quit(1)

if LINUX:
    USB_serial_numbers = [USB_serial_number[:-1] for USB_serial_number in USB_serial_numbers]

all_ports = serial.tools.list_ports.comports()

printb('\nchecking USB ports for boards')
printb('\n<Serial #>\t<message>\t\t\t<reply>\t\t\t\t<status>')
for port in all_ports:
    try:
        connection = None
        if LINUX:
            connection = serial.Serial(port=port.device, baudrate=9600, timeout=1, write_timeout=1)
        else:
            connection = serial.Serial(port=port.name, baudrate=9600, timeout=1, write_timeout=1)

        message = "*IDN?\r\n".encode()
        connection.write(message)
        reply = connection.readline()
        printb(f'{port.serial_number}\t{str(message):<24}\t{str(reply):<24}', end='')

        if port.serial_number in USB_serial_numbers:
            USB_idx = USB_serial_numbers.index(port.serial_number)
            if IDN_serial_numbers[USB_idx] == reply:
                ports[USB_idx] = port
                connections[USB_idx] = connection
                printb(f'\tFound board (zero indexed) #{USB_idx}')
            else:
                printb(f'\tRight USB serial number, wrong IDN serial number: expected IDN serial number {IDN_serial_numbers[USB_idx]}')
                
        else:
            printb(f'\tWrong USB serial number: expected {USB_serial_numbers}')
            connection.close()
        
    except Exception as e:
         # printb(port, e)
        printb(f'could not open connection to port {port}')
        # exit(1)

# if None in ports:
for i in range(len(ports)):
    if isinstance(ports[i], type(None)):
        printb(f'board #{i} not found, exiting')
        exit(1)

printb('\nFound all boards')

def write_to_BNC(port, connection, message):
    connection.write((message + '\r\n').encode())
    reply = connection.readline()
    printb(f'{port.serial_number}\t{str(message):<24}\t{str(reply):<24}', end='')

    return reply

for board_idx in range(len(ports)):
    commands = commands_list[board_idx]
    port = ports[board_idx]
    connection = connections[board_idx]

    printb(f'\ncommunicating with board {board_idx}, USB serial number {USB_serial_numbers[board_idx]}\n')
    printb('<Serial #>\t<message>\t\t\t<reply>\t\t\t\t<status>')
    for command in commands:
        i = 0
        reply = ''
        while i < 5:
            time.sleep(0.1)
            reply = write_to_BNC(port, connection, command)

            if reply == OK_REPLY:
                printb('\tOK')
                break

            i += 1
            printb(f'\tNOT OK trying again ({i}/5)')
            
        if reply != OK_REPLY:
            printb(f'########previous command {command} failed after 5 tries, exiting')
            exit(1)
    
    printb(f'\nfinished with board {board_idx}, USB serial number {USB_serial_numbers[board_idx]}, closing connection')
    connection.close()

printb('\nScript completed successfully :)))')