#!/bin/bash

#Argument 1 is the input directory
#Argument 2 is the output directory
#Argumnet 3 is the output file tag (File will be named tagRun#.root)
#Run with $./batchprocess.sh /path/to/files/

argc=("$#")

echo $argc

if("$argc" -eq 0); then
    echo "Please enter a source file path at the very least"
    exit
fi

if("$argc" -eq 1); then
    $2="./"
    $3="output"
fi

if("$argc" -eq 2); then
    $3="output"
fi


for filename in $1/*.prdf; do
    root -b -q "ParsePRDF.cxx(\"$filename\",\"$2\",\"$3\")"
done

