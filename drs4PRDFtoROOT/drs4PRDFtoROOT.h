#ifndef __DRS4PRDFTOROOT_H__
#define __DRS4PRDFTOROOT_H__

#include <TFile.h>
#include <TTree.h>

#include <iostream>
#include <vector>
#include <string>

#include <Event/Event.h>
#include <Event/EventTypes.h>

using namespace std;

int setOutputFilePath(string outPath);
void setOutputTag(string _tag);
void setMaxEvents(int _max);
int process_event (Event *e); //++CINT

#endif /* __DRS4PRDFTOROOT_H__ */
