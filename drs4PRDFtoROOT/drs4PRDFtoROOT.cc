#include <pmonitor/pmonitor.h>
#include <algorithm>
#include "drs4PRDFtoROOT.h"
#include "TSystem.h"

int init_done = 0;


TFile *fOut;
TTree *tOut;
vector< vector< float > > vData;
vector< vector< float > > vTime;
vector< int > devID;
int nDevices = 0;
int EventNum,RunNum;
int maxEvents = -1;

string foutPath = "./";
string fTag = "output";

//TODO: Enable channel disabling through .txt file

int setOutputFilePath(string outPath){ foutPath = outPath; }
void setOutputTag(string _tag){ fTag = _tag; }
void setMaxEvents(int _max){ maxEvents = _max; }
void createTree(Event *e);
void fillTree(Event* e);
void clearVectors();
void freeMemory();
void updateTerminal();


int pinit()
{

  if (init_done) return 1;
  init_done = 1;

  return 0;

}

int process_event (Event * e)
{

  //Skip or null pointer, begin-run (type=9), Data event (type=0), Mystery type (type=601)
  if (e == 0 || e->getEvtType() == 9 || e->getEvtType() == 0 || e->getEvtType() == 601) return 0;

  RunNum = e->getRunNumber();
  EventNum = e->getEvtSequence();
  if (EventNum % 100 == 0) updateTerminal();

  //If it's the last event, write the output file and exit
  if (e->getEvtType() == 12 || EventNum == maxEvents ){
    tOut->Write();
    fOut->Close();
    freeMemory();
    return 0;
  }
  //If it's the first event with data create the tree and branches for the data
  if( e->getEvtSequence() == 2 ){
    createTree(e);
  }

  //Finally, fill the vectors for this event and fill the tree
  fillTree(e);
  clearVectors();

  return 0;
}

/*
 *  Search through the packets in the event and create a tree with enough
 *  branches for all of the DRS4 modules contained within.
 *
 */
void createTree(Event *e){
  int arrLen = 30;
  Packet *p[arrLen];
  Packet *pack;
  nDevices = e->getPacketList(p, arrLen);

  if(nDevices == 0) return;

  fOut = new TFile( Form("%s/%s%d.root", foutPath.c_str(), fTag.c_str(), e->getRunNumber()), "recreate" );
  tOut = new TTree( "tree", "tree" );
  tOut->Branch("RunNumber", &RunNum);
  tOut->Branch("EventNumber", &EventNum);

  //Sort the device ids of DRS4s (HitFormat 81 = DRS4)
  for(int dev = 0; dev < nDevices; dev++)
    if( p[dev]->getHitFormat() == 81 ) devID.push_back( p[dev]->getIdentifier() );

  sort(devID.begin(), devID.begin()+nDevices);

  //Resize nDevices to be the number of DRS4s in the file
  nDevices = devID.size();

  vData.resize(4*nDevices);
  vTime.resize(nDevices);
  for(int dev = 0; dev < nDevices; dev++){
    for(int ch = 0; ch < 4; ch++ ){
      tOut->Branch( Form( "RawSignal%d", 4*dev + ch), "std::vector<float>", &vData[4*dev + ch] );
    }
    tOut->Branch( Form( "time%d", dev), "std::vector<float>", &vTime[dev] );
  }
}

/*
 *  Fill the waveform and time vectors and call TTree::Fill()
 *
 */
void fillTree(Event* e){
  Packet *p;
  for(int dev = 0; dev < nDevices; dev++){
    p = e->getPacket( devID[dev] );
    for(int ch = 0; ch < 4; ch++){
      for(int bin = 0; bin < 1024; bin++){
        vData[4*dev + ch].push_back( p->rValue(bin, ch) );
      }
    }
    for(int bin = 0; bin < 1024; bin++){
      vTime[dev].push_back( p->rValue(bin, 4) );
    }
  }
  tOut->Fill();
}

/*
 * Clear the vectors in anticipation of the next event
 *
 */
void clearVectors(){
  for( int i = 0; i < vData.size(); i++ ) vData[i].clear();
  for( int i = 0; i < vTime.size(); i++ ) vTime[i].clear();
}

void updateTerminal(){
    MemInfo_t memInfo;
    CpuInfo_t cpuInfo;

    // Get CPU information
    gSystem->GetCpuInfo(&cpuInfo, 100);
    // Get Memory information
    gSystem->GetMemInfo(&memInfo);

    // Print the number of events processed, CPU and RAM utilization
    cout << "\r" << left;
    cout << Form( "Processed %d events, ", EventNum);
    cout << Form( "CPU: %d", (int)cpuInfo.fTotal) << "%, ";
    cout << Form( "RAM:%3.1f/%3.1fGB", (double)memInfo.fMemUsed/1024, (double)memInfo.fMemTotal/1024) << flush;
}


/*
 * Delete all objects not owned by the TFile
 *
 */
void freeMemory(){

}
