#! /bin/sh

#Get DRS4 samples for 10 seconds
x=10


sh /data/phenix/drs_setup/drs_setup.sh

rcdaq_client daq_open

rcdaq_client daq_begin

echo Running for $x seconds

for i in `seq 1 $x`
do
sleep 1s
echo $i
done
rcdaq_client daq_status

rcdaq_client daq_end

rcdaq_client daq_close
rcdaq_client daq_shutdown
