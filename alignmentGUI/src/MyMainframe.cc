/// \file
/// \ingroup tutorial_gui
/// This macro gives an example of different buttons' layout.
/// To run it do either:
/// ~~~
/// .x buttonsLayout.C
/// .x buttonsLayout.C++
/// ~~~
///
/// \macro_code
///
/// \author Chad Lantz 8 August 2021



#include <TGClient.h>
#include <TGButton.h>
#include <TGLabel.h>
#include <TGMsgBox.h>
#include <TSystem.h>
#include <TDatime.h>

#include <iostream>
#include <string>
#include <sstream>

#include "MyMainFrame.hh"
#include "T4XMLWriter.hh"


using namespace std;


MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h) :
  TGMainFrame(p, w, h)
{
   // Create a container frames containing buttons

   // one button is resized up to the parent width.
   // Note! this width should be fixed!
   TGHorizontalFrame *hframe1 = new TGHorizontalFrame(this, w, 50, kFixedWidth);

   hframe1->AddFrame(new TGLabel(hframe1, new TGHotString("Table X: [mm]")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   hframe1->AddFrame(new TGLabel(hframe1, new TGHotString("Table Y: [mm]")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   hframe1->AddFrame(new TGLabel(hframe1, new TGHotString("Trigger X: [T.U.]")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   hframe1->AddFrame(new TGLabel(hframe1, new TGHotString("Trigger Y: [T.U.]")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   hframe1->AddFrame(new TGLabel(hframe1, new TGHotString("Abs Depth: [mm]")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   hframe1->AddFrame(new TGLabel(hframe1, new TGHotString("Beam Type:")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   hframe1->AddFrame(new TGLabel(hframe1, new TGHotString("Beam Energy: [GeV]")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   // to take whole space we need to use kLHintsExpandX layout hints
   AddFrame(hframe1, new TGLayoutHints(kLHintsCenterX, 2, 2, 5, 1));



   /////////////////////////////////
   /////      Middle Row
   /////////////////////////////////

   // two buttons are resized up to the parent width.
   // Note! this width should be fixed!
   TGCompositeFrame *cframe2 = new TGCompositeFrame(this, w, 25,
                                             kHorizontalFrame | kFixedWidth);

   TGNumberFormat::EStyle style = TGNumberFormat::kNESReal;
   TGNumberFormat::EAttribute attr = TGNumberFormat::kNEAAnyNumber;
   TGNumberFormat::ELimit lim;

   //Table X
   NumTblX = new TGNumberEntry(cframe2, 1, 12);
   NumTblX->SetFormat( style, attr );
   NumTblX->SetLimitValues( 1, 10 );
   cframe2->AddFrame(NumTblX, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   //Table Y
   NumTblY= new TGNumberEntry(cframe2, 1, 12);
   NumTblY->SetFormat( style, attr );
   NumTblY->SetLimitValues( 1, 10 );
   cframe2->AddFrame(NumTblY, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   //Trigger X
   NumTrgX = new TGNumberEntry(cframe2, 1, 12);
   NumTrgX->SetFormat( style, attr );
   NumTrgX->SetLimitValues( 1, 10 );
   cframe2->AddFrame(NumTrgX, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   //Trigger Y
   NumTrgY = new TGNumberEntry(cframe2, 1, 12);
   NumTrgY->SetFormat( style, attr );
   NumTrgY->SetLimitValues( 1, 10 );
   cframe2->AddFrame(NumTrgY, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   //Absorber Thickness
   NumAbs = new TGNumberEntry(cframe2, 1, 12);
   NumAbs->SetFormat( style, attr );
   NumAbs->SetLimitValues( 1, 10 );
   cframe2->AddFrame(NumAbs, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   //Beam Type
   Beam = new TGComboBox(cframe2,1);//2nd argument is ID number
   Beam->AddEntry("electron",1);
   Beam->AddEntry("proton",2);
   cframe2->AddFrame(Beam, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );
   Beam->Resize(70,20);

   //Absorber Thickness
   NumBeamE = new TGNumberEntry(cframe2, 1, 12);
   NumBeamE->SetFormat( style, attr );
   NumBeamE->SetLimitValues( 1, 10 );
   cframe2->AddFrame(NumBeamE, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );


   AddFrame(cframe2, new TGLayoutHints(kLHintsCenterX, 2, 2, 5, 1));



   ///More names
   TGCompositeFrame *cframe3 = new TGCompositeFrame(this, w, 20,
     kHorizontalFrame | kFixedWidth);


   cframe3->AddFrame(new TGLabel(cframe3, new TGHotString("Det 1:")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   cframe3->AddFrame(new TGLabel(cframe3, new TGHotString("Det 2:")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   cframe3->AddFrame(new TGLabel(cframe3, new TGHotString("Det 3:")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   cframe3->AddFrame(new TGLabel(cframe3, new TGHotString("Det 4:")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   cframe3->AddFrame(new TGLabel(cframe3, new TGHotString("Det 5:")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   cframe3->AddFrame(new TGLabel(cframe3, new TGHotString("Det 6:")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );

   AddFrame(cframe3, new TGLayoutHints(kLHintsCenterX, 2, 2, 5, 1));


  //Detectors
   TGCompositeFrame *cframe4 = new TGCompositeFrame(this, w, 30,
     kHorizontalFrame | kFixedWidth);

     vector< string > detector = {"","CMS EM CAL","Prototype EM Cal", "PF RPD", "Tile RPD", "ATLAS HAD", "LHCf", "ADAMO"};
     //Detectors
     for(int det = 0; det < 6; det++){
       Det[det] = new TGComboBox(cframe4,det+2);
       for(int type = 0; type < detector.size(); type++){
         Det[det]->AddEntry(detector[type].c_str(),type+1);
       }
       cframe4->AddFrame(Det[det], new TGLayoutHints(kLHintsTop | kLHintsExpandX, 5, 5, 0, 0) );
       Det[det]->Resize(70,20);
     }

     AddFrame(cframe4, new TGLayoutHints(kLHintsCenterX, 2, 2, 5, 1));




   /////////////////////////////////
   /////      Bottom Row
   /////////////////////////////////
   // three buttons are resized up to the parent width.
   // Note! this width should be fixed!
   TGCompositeFrame *cframe5 = new TGCompositeFrame(this, 550, 20,
                                             kHorizontalFrame | kFixedWidth);

   SetupBtn = new TGTextButton(cframe5, "Setup DAQ");
   SetupBtn->Connect("Clicked()", "MyMainFrame", this, "setupDaq()");
   // to share whole parent space we need to use kLHintsExpandX layout hints
   cframe5->AddFrame(SetupBtn, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
                                           3, 2, 2, 2));


   OpenBtn = new TGTextButton(cframe5, "Open DAQ");
   OpenBtn->Connect("Clicked()", "MyMainFrame", this, "openDaq()");
   // to share whole parent space we need to use kLHintsExpandX layout hints
   cframe5->AddFrame(OpenBtn, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
                                           3, 2, 2, 2));

   CloseBtn = new TGTextButton(cframe5, "Close DAQ");
   CloseBtn->Connect("Clicked()", "MyMainFrame", this, "closeDaq()");
   // to share whole parent space we need to use kLHintsExpandX layout hints
   cframe5->AddFrame(CloseBtn, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
                                           3, 2, 2, 2));

   AddBtn = new TGTextButton(cframe5, "Begin Run");
   AddBtn->Connect("Clicked()", "MyMainFrame", this, "ElogAndXMLOnly()");
   // AddBtn->Connect("Clicked()", "MyMainFrame", this, "AddEntry()");
   // to share whole parent space we need to use kLHintsExpandX layout hints
   cframe5->AddFrame(AddBtn, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
                                           3, 2, 2, 2));

   EndBtn = new TGTextButton(cframe5, "End Run");
   EndBtn->Connect("Clicked()", "MyMainFrame", this, "endRun()");
   // to share whole parent space we need to use kLHintsExpandX layout hints
   cframe5->AddFrame(EndBtn, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
                                           3, 2, 2, 2));


   exit = new TGTextButton(cframe5, "&Exit ","gApplication->Terminate(0)");
   cframe5->AddFrame(exit, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
                                             2, 0, 2, 2));

   //Run Number
   cframe5->AddFrame(new TGLabel(cframe5, new TGHotString("Run#")), new TGLayoutHints(kLHintsTop | kLHintsExpandX,2, 2, 2, 2) );
   RunNum= new TGNumberEntry(cframe5, 1, 12);
   RunNum->SetFormat( TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive );
   RunNum->SetLimitValues( 1, 10 );
   cframe5->AddFrame(RunNum, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2) );

   AddFrame(cframe5, new TGLayoutHints(kLHintsCenterX, 2, 2, 5, 1));

   /////////////////////////////////
   /////      Status Bar
   /////////////////////////////////
   //Create a TTimer to get status every 3 seconds
   Timer = new TTimer( this, 3000);

   StatusBar = new TGStatusBar(this, 50, 10, kVerticalFrame);
   StatusBar->Draw3DCorner(kFALSE);
   AddFrame(StatusBar, new TGLayoutHints(kLHintsExpandX, 0, 0, 10, 0));

   SetWindowName("UIUC Rules!!!");

   // gives min/max window size + a step of x,y incrementing
   // between the given sizes
   SetWMSizeHints(500, 100, 1920, 1080, 1, 1);
   MapSubwindows();
   // important for layout algorithm
   Resize(GetDefaultSize());
   MapWindow();
}


MyMainFrame::~MyMainFrame()
{
   // Clean up all widgets, frames and layouthints that were used
   Cleanup();
}

void MyMainFrame::ElogAndXMLOnly(){

  TString fileName = WriteXML( RunNum->GetNumber(), NumTblX->GetNumber(), NumTblY->GetNumber());
  string output = Form("Run %.0f started", RunNum->GetNumber());
  RunNum->SetNumber( RunNum->GetNumber() + 1 );
  StatusBar->SetText( output.c_str() );
  gSystem->Exec( Form("elog -h localhost -p 8080 -l RCDAQLog -a Author=RCDAQ -a  Type=Software -a Category=Runs -a Subject=RCDAQ_Start_Run \"%s\" -f %s", output.c_str(), fileName.Data() ) );
  gSystem->Exec(Form("rm %s",fileName.Data() ) );

}

void MyMainFrame::AddEntry(){


  // string output = gSystem->GetFromPipe("ssh pi@h2raspitablepos.cern.ch tmux a -t\"Table Disto\"").Data();
  string output = "no sessions";
  //Copy last 20 lines into elog file
  //Extract last entry for x_table and y_table filling
  double table_x = 0.0;
  double table_y = 0.0;
  size_t lastH, lastV;
  if(output == "no sessions" || output.size() < 500){//If the raspberry pi failed
    if(NumTblX->GetNumber() == 1){//And the user hasn't manually entered a number
      Disconnect("CloseWindow()");

      new TGMsgBox(gClient->GetRoot(), this, "Table Position Failure",
                   "Please enter the current table position in the box",
                   kMBIconExclamation, kMBOk);

      //Reconnect the close window button once the error is acknowledged
      Connect("CloseWindow()", "MyMainFrame", this, "CloseWindow()");
      return;
    }else{//The user entered a number, use it
      table_x = NumTblX->GetNumber();
      table_y = NumTblY->GetNumber();
    }
  }else{//The raspberry pi worked
    lastH = output.find_last_of("H");
    table_x = atof( output.substr( lastH + 3, lastH + 8).c_str() );

    lastV = output.find_last_of("V");
    table_y = atof( output.substr( lastV + 3, lastV + 8).c_str() );
  }

  //Begin a run, send a copy of the output to elog, and extract the run number
  output = gSystem->GetFromPipe("rcdaq_client daq_begin").Data();

  if(output == "Run is already active"){
     Disconnect("CloseWindow()");

      new TGMsgBox(gClient->GetRoot(), this, "A run is already active",
                   "Please end the current run before beginning a new one",
                   kMBIconExclamation, kMBOk);

      //Reconnect the close window button once the error is acknowledged
      Connect("CloseWindow()", "MyMainFrame", this, "CloseWindow()");
      return;
  }
  Timer->TurnOn();

  //Set the status bar text with the run started message
  StatusBar->SetText( output.c_str() );

  size_t firstSpace, secondSpace;
  firstSpace = output.find_first_of(" ");
  secondSpace = output.find_first_of(" ", firstSpace + 1);
  int runNo = atoi( output.substr( firstSpace, secondSpace - firstSpace ).c_str() );
  currentRunNo = runNo;

  TString fileName = WriteXML( runNo, table_x, table_y );

  gSystem->Exec( Form("elog -h localhost -p 8080 -l RCDAQLog -a Author=RCDAQ -a  Type=Software -a Category=Runs -a Subject=RCDAQ_Start_Run \"%s\" -f %s", output.c_str(), fileName.Data() ) );
  gSystem->Exec(Form("rm %s",fileName.Data() ) );

}




TString MyMainFrame::WriteXML( int runNo, double table_x, double table_y ){

    T4XMLWriter *myWriter = new T4XMLWriter();
    myWriter->addNode("Alignment");
    myWriter->createDataNode("Alignment",0,"run", runNo);
    myWriter->createDataNode("Alignment",0,"x_table", table_x);
    myWriter->createDataNode("Alignment",0,"y_table", table_y);
    myWriter->createDataNode("Alignment",0,"x_trigger",NumTrgX->GetNumber());
    myWriter->createDataNode("Alignment",0,"y_trigger",NumTrgY->GetNumber());
    myWriter->createDataNode("Alignment",0,"abs_depth",NumAbs->GetNumber());
    myWriter->createDataNode("Alignment",0,"beam_energy",NumBeamE->GetNumber());

    //Get Beam type
    if(Beam->GetSelected() == -1){
      Disconnect("CloseWindow()");

      new TGMsgBox(gClient->GetRoot(), this, "No Beam Type selected",
                   "Please select a beam type before adding an entry",
                   kMBIconExclamation, kMBOk);

      //Reconnect the close window button once the error is acknowledged
      Connect("CloseWindow()", "MyMainFrame", this, "CloseWindow()");
      return "";
    }else if(Beam->GetSelected() == 1){ //electron

      myWriter->createDataNode("Alignment",0,"beam_type", (string)"e");

    }else if(Beam->GetSelected() == 2){ //protron

      myWriter->createDataNode("Alignment",0,"beam_type", (string)"p");

    }

    //Write Detectors
    vector< string > detector = {"","EM CAL","CMS Prototype EM Cal", "PF RPD", "Tile RPD", "ATLAS HAD", "LHCf", "ADAMO"};
    for(int det = 0; det < 6; det++){
        switch(Det[det]->GetSelected()) {
      case -1://Nothing was selected
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"none");
        break;
      case 1://Blank option was selected
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"none");
        break;
      case 2://CMS EM Cal
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"UEM");
        break;
      case 3://CMS Prototype EM Cal
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"PUEM");
        break;
      case 4://Pan Flute RPD
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"PFRPD");
        break;
      case 5://Tile RPD
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"TRPD");
        break;
      case 6://ATLAS Hadronic module
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"ZDC");
        break;
      case 7://LHCf module
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"LHCF");
        break;
      case 8://ADAMO tracker
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"ADAMO");
        break;
      default:
        myWriter->createDataNode("Alignment",0,Form("Det%d",det+1), (string)"none");
      }//End switch
    }//End detector loop

    TDatime time;
    string timeStr = time.AsSQLString();
    timeStr.replace( timeStr.find_first_of(" "), 1, "_");
    TString fileName = Form("%s_alignment.xml", timeStr.c_str() );
    myWriter->createFile( fileName.Data() );
    delete myWriter;
    return fileName;
}


void MyMainFrame::endRun(){

  StatusBar->SetText( gSystem->GetFromPipe("rcdaq_client daq_end").Data() );

}

void MyMainFrame::setupDaq(){

  StatusBar->SetText( gSystem->GetFromPipe("/home/bgu-rhi/DAQ_2021/Production/setupRCDAQ.sh") );

}


void MyMainFrame::openDaq(){

  gSystem->Exec("rcdaq_client daq_open");
  gSystem->Sleep(500);
  StatusBar->SetText( gSystem->GetFromPipe("rcdaq_client daq_status") );

}

void MyMainFrame::closeDaq(){

  gSystem->Exec("rcdaq_client daq_close");
  gSystem->Sleep(500);
  StatusBar->SetText( gSystem->GetFromPipe("rcdaq_client daq_status") );

}

/*
 * Update the status bar
 */
Bool_t MyMainFrame::HandleTimer( TTimer *timer ){

  TString output = gSystem->GetFromPipe("rcdaq_client daq_status").Data();
  StatusBar->SetText( output.Data() );

  //Run is ongoing, update status bar
  if( output.Contains("Run ") ){
    StatusBar->SetText( output.Data() );
  }else{ // Run has ended
    StatusBar->SetText( Form("Run %d completed",currentRunNo) );
    Timer->TurnOff();
  }

  return true;

}
