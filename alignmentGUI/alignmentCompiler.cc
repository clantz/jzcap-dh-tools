/****************************************************
*
* Alignment file compiler for the 2021 JZCaP Test Beam
*
* Usage:
* ./compileXML /path/to/files outputFileName
*
* This program will find all .xml files in a given directory
* and compile the contents into a single file
*
****************************************************/


#include <iostream>
#include <fstream>

#include "TString.h"
#include "TSystem.h"
#include "TSystemDirectory.h"
#include "TList.h"
#include "TFile.h"
#include "TH2F.h"

#include "XMLSettingsReader.hh"
#include "T4XMLWriter.hh"

using namespace std;

namespace {
  void PrintUsage() {
    cerr << " Usage: " << endl;
    cerr << " compileXML [-a /path/to/source /output/file.xml ] [-c /path/to/source.csv /output/file.xml]"
           << endl;
  }
}

void doAlignments(string inputDir, string _outFile);
void doConfigs(string _inFile, string _outFile);
Alignment2021* LoadAlignmentFile(string _inFile );
vector< TString > list_files(const char *dirname="C:/root/folder/", const char *ext=".root");
string getDataPoint( string &input, size_t spaces = 1, string delim = "," );

vector<int> wrongRun = {  868,   869,   871,   872,   873,   874,   876,   877,   878,   879,   880,   881,   882,   883,   884,   885,   886,   887,   888,   890,   891,   892,   893,   894,   895,   896,   897,   898,   899,   900,   901,   902,   903,   904,   905,   906,   907,   908,   909,   910,   911,   913 };
vector<int> rightRun = {70069, 70067, 70070, 70071, 70072, 70073, 70074, 70075, 70076, 70077, 70078, 70079, 70080, 70081, 70082, 70083, 70084, 70085, 70086, 70108, 70111, 70112, 70113, 70135, 70136, 70137, 70138, 70140, 70144, 70145, 70146, 70147, 70148, 70149, 70150, 70173, 70174, 70175, 70176, 70178, 70179, 70180 };


/*
 * Compile all .xml files containing alignment
 */
int main(int argc, char** argv){

  string input, outputFile;
  TString testStr;
  if(argc == 4){
    input = argv[2];
    outputFile = argv[3];
  }else{
    cout << "Undefined behavior: Exiting" << endl;
    return -1;
  }

  testStr = outputFile;
  testStr.ToLower();
  if(!testStr.Contains(".xml")) outputFile += ".xml";

  if     ( TString(argv[1]) == "-a" ) doAlignments(input, outputFile);
  else if( TString(argv[1]) == "-c" ) doConfigs(input, outputFile);


  //Append output file name with .xml if the user didn't include it

}//end main


void doAlignments(string inputDir, string _outFile){

    //Retrieve alignments from each file into alignment objects
    vector< Alignment2021* > entries;
    vector< TString > file_list = list_files( inputDir.c_str(), ".xml");
    for( TString file : file_list ){
      entries.push_back( LoadAlignmentFile( (inputDir + file).Data() ) );
    }//end file loop

    //Create a new file with all of the alignments
    T4XMLWriter *myWriter = new T4XMLWriter();
    int run;
    for( int alignment = 0; alignment < entries.size(); alignment++){
      if( entries[alignment] == NULL ){
        cout << "Error reading alignment file" << endl;
        return;
      }
      for(int check = 0; check < wrongRun.size(); check ++){
        if(entries[alignment]->runNumber == wrongRun[check]){
          cout << "Changing run " << entries[alignment]->runNumber << " to " << rightRun[check] << endl;
          run = rightRun[check];
          break;
        }else{
          run  = entries[alignment]->runNumber;
        }
      }
      myWriter->addNode("Alignment");
      myWriter->createDataNode("Alignment",alignment,"run",         run                            );
      myWriter->createDataNode("Alignment",alignment,"x_table",     entries[alignment]->x_det_table);
      myWriter->createDataNode("Alignment",alignment,"y_table",     entries[alignment]->y_det_table);
      myWriter->createDataNode("Alignment",alignment,"x_trigger",   entries[alignment]->x_trig_table);
      myWriter->createDataNode("Alignment",alignment,"y_trigger",   entries[alignment]->y_trig_table);
      myWriter->createDataNode("Alignment",alignment,"abs_depth",   entries[alignment]->absorber);
      myWriter->createDataNode("Alignment",alignment,"beam_energy", entries[alignment]->beam_energy);
      myWriter->createDataNode("Alignment",alignment,"beam_type",   entries[alignment]->beam_type);
      myWriter->createDataNode("Alignment",alignment,"Det1",        entries[alignment]->Det1);
      myWriter->createDataNode("Alignment",alignment,"Det2",        entries[alignment]->Det2);
      myWriter->createDataNode("Alignment",alignment,"Det3",        entries[alignment]->Det3);
      myWriter->createDataNode("Alignment",alignment,"Det4",        entries[alignment]->Det4);
      myWriter->createDataNode("Alignment",alignment,"Det5",        entries[alignment]->Det5);
      myWriter->createDataNode("Alignment",alignment,"Det6",        entries[alignment]->Det6);
    }
    myWriter->createFile( _outFile.c_str() );
    delete myWriter;

}


void doConfigs(string _inFile, string _outFile){

  ifstream file(_inFile);
  if( !file.is_open() ){
    cout << "File didn't open" << endl;
    return;
  }
  string buffer;
  //Burn the header
  getline(file, buffer);

  T4XMLWriter *myWriter = new T4XMLWriter();
  vector< Config2021* > configs;
  int line = 0;
  int firstRun = 100000;
  int lastRun = 0;

  //Get the first line
  getline(file, buffer);
  while( buffer.size() > 5 ){
    Config2021* cBuff = new Config2021;
    //csv is structured as follows
    //name,detector,start_run,end_run,hv_set,is_on,mapping_row,mapping_col,pmt_code,polarity

    cBuff->name = getDataPoint( buffer, 1 );
    cBuff->detector = getDataPoint( buffer, 1 );
    cBuff->start_run = atoi( getDataPoint( buffer, 1 ).c_str() );
    cBuff->end_run = atoi( getDataPoint( buffer, 1 ).c_str() );
    cBuff->hv_set = atof( getDataPoint( buffer, 1 ).c_str() );
    cBuff->is_on = (getDataPoint( buffer, 1 ).compare("0") == 0)? false : true;
    cBuff->mapping_row = atoi( getDataPoint( buffer, 1 ).c_str() );
    cBuff->mapping_col = atoi( getDataPoint( buffer, 1 ).c_str() );
    cBuff->pmt_code = getDataPoint( buffer, 1 );
    cBuff->pos_polarity = (getDataPoint( buffer, 1 ).compare("negative") == 0) ? false : true ;

    configs.push_back(cBuff);

    if(cBuff->start_run < firstRun) firstRun = cBuff->start_run;
    if(cBuff->end_run > lastRun) lastRun = cBuff->end_run;

    myWriter->addNode("channel");
    myWriter->createDataNode("channel",line,"name",             cBuff->name );
    myWriter->createDataNode("channel",line,"detector",         cBuff->detector );
    myWriter->createDataNode("channel",line,"start_run",        cBuff->start_run );
    myWriter->createDataNode("channel",line,"end_run",          cBuff->end_run );
    myWriter->createDataNode("channel",line,"hv_set",           cBuff->hv_set );
    myWriter->createDataNode("channel",line,"is_on",            cBuff->is_on );
    myWriter->createDataNode("channel",line,"mapping_row",      cBuff->mapping_row );
    myWriter->createDataNode("channel",line,"mapping_column",   cBuff->mapping_col );
    myWriter->createDataNode("channel",line,"pmt_code",         cBuff->pmt_code );
    myWriter->createDataNode("channel",line,"is_pos_polarity",  cBuff->pos_polarity );
    myWriter->createDataNode("channel",line,"adc_per_mv",       0.244 );

    getline(file,buffer);
    line++;
  }

  TH2F *h = new TH2F("tl","Timeline;Run Number; Channel name; # occurences",lastRun - firstRun, firstRun, lastRun, 2,0,2);

  for(Config2021 *conf : configs){
    for(int run = conf->start_run; run <= conf->end_run; run++){
      h->Fill(run,conf->name.c_str(),1);
    }
  }

  if(h->GetMaximum() > 1){
    cout << "REDUNDANCIES DETECTED!!!" << endl << "Please review configs.root to identify the issue" << endl;
    TFile f("configs.root","recreate");
    h->Write();
    f.Close();
  }


  myWriter->createFile( _outFile.c_str() );
  delete myWriter;

}




/**
 * @brief Reads the .xml configuration file and load characteristics for all the channels, immediately sorted into detectors objects
 * @param _inFile
 */
Alignment2021* LoadAlignmentFile(string _inFile ){

    XMLSettingsReader *XMLparser = new XMLSettingsReader();

    if (!XMLparser->parseFile(_inFile)) {
            cerr << " Data Reader could not parse file : " << _inFile << endl;
            return NULL;
    }

    Alignment2021 *alignment = new Alignment2021();
    if(XMLparser->getBaseNodeCount("Alignment") != 1){
      cout << "Wrong number of nodes in " << _inFile << endl;
      return NULL;
    }
    for (unsigned int i = 0; i < XMLparser->getBaseNodeCount("Alignment"); i++) {
        XMLparser->getChildValue("Alignment",i,"run",alignment->runNumber);
        XMLparser->getChildValue("Alignment",i,"beam_type",alignment->beam_type);
        XMLparser->getChildValue("Alignment",i,"beam_energy",alignment->beam_energy);
        XMLparser->getChildValue("Alignment",i,"abs_depth",alignment->absorber);
        XMLparser->getChildValue("Alignment",i,"x_table",alignment->x_det_table);
        XMLparser->getChildValue("Alignment",i,"y_table",alignment->y_det_table);
        XMLparser->getChildValue("Alignment",i,"x_trigger",alignment->x_trig_table);
        XMLparser->getChildValue("Alignment",i,"y_trigger",alignment->y_trig_table);
        XMLparser->getChildValue("Alignment",i,"Det1",alignment->Det1);
        XMLparser->getChildValue("Alignment",i,"Det2",alignment->Det2);
        XMLparser->getChildValue("Alignment",i,"Det3",alignment->Det3);
        XMLparser->getChildValue("Alignment",i,"Det4",alignment->Det4);
        XMLparser->getChildValue("Alignment",i,"Det5",alignment->Det5);
        XMLparser->getChildValue("Alignment",i,"Det6",alignment->Det6);
    }

    if(alignment == NULL) cout << "WARNING: ALIGNMENT NOT FOUND!!!" << endl;
    delete XMLparser; XMLparser = NULL;
    return alignment;

}


/**
 * @brief Lists all files in a given directory with the given extension
 */
vector< TString > list_files(const char *dirname, const char *ext) {
  vector< TString > list;
  TSystemDirectory dir(dirname, dirname);
  TList *files = dir.GetListOfFiles();

  if (files) {
    TSystemFile *file;
    TString fname;
    TIter next(files);
    while ((file=(TSystemFile*)next())) {
      fname = file->GetName();
      if (!file->IsDirectory() && fname.EndsWith(ext)) {
        cout << fname.Data() << endl;
        list.push_back( fname );
      }
    }
  }
  return list;
}

/**
 * @brief Gets one data point from a csv line then erases that point from the line
 */
string getDataPoint( string &input, size_t spaces, string delim ){
  size_t firstComma = input.find_first_of(delim.c_str());
  string buffer = input.substr(0, firstComma );
  input.erase(0,firstComma+spaces);
  if(buffer == "") buffer = "nan";
  return buffer;
}
