#ifndef MYMAINFRAME_HH_
#define MYMAINFRAME_HH_

#include <TGFrame.h>
#include <TGTab.h>
#include <TGButton.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGStatusBar.h>
#include <TTimer.h>

class TGWindow;
class TRootEmbeddedCanvas;

class MyMainFrame : public TGMainFrame {

private:
   TGTextButton *SetupBtn, *OpenBtn, *CloseBtn, *AddBtn, *EndBtn, *exit;
   TGNumberEntry *NumTblX, *NumTblY, *NumTrgX, *NumTrgY, *NumAbs, *NumBeamE, *RunNum;
   TGComboBox *Beam, *Det[6];
   TGStatusBar *StatusBar;
   TTimer *Timer;
   int currentRunNo;

public:
   MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h);
   virtual ~MyMainFrame();

   void AddEntry();
   void ElogAndXMLOnly();
   void endRun();
   void openDaq();
   void closeDaq();
   void setupDaq();
   TString WriteXML( int runNo, double table_x, double table_y );
   Bool_t HandleTimer( TTimer *timer );


   ClassDef(MyMainFrame, 0)
};


#endif /* MYMAINFRAME_HH_ */
