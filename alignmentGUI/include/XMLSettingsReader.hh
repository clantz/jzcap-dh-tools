/** @file XMLSettingsReader
 *  @brief Function prototypes for XMLSettingsReader
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef XMLSETTINGSREADER_HH
#define XMLSETTINGSREADER_HH

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include <string>
#include <vector>
#include <sstream>
#include <iostream>

using namespace xercesc;

class Config2021{

  public:
      /** Channel name */
      std::string name;
      /** Detector name */
      std::string detector;
      /** First run this config is valid for */
      int start_run;
      /** Last run this config is valid for */
      int end_run;
      /** High voltage setting for this channle */
      float hv_set;
      /** If the channel was in use */
      bool is_on;
      /** Row in the detector */
      int mapping_row;
      /** Column in the detector */
      int mapping_col;
      /** PMT serial number */
      std::string pmt_code;
      /** Polarity of the signal */
      bool pos_polarity;
};


class Alignment2021 {

 public:

    /** Run number being analyzed **/
    int runNumber;
    /** Beam type - can be p or e in H2 2021 **/
    std::string beam_type;
    /** Beam energy **/
    double beam_energy;
    /** Absorber thickness **/
    double absorber;
    /** X position of the NIKHEF Table **/
    double x_det_table;
    /** Y position of the NIKHEF Table **/
    double y_det_table;
    /** X position of the Trigger Table **/
    double x_trig_table;
    /** Y position of the Trigger Table **/
    double y_trig_table;
    /** First detector met by the beam **/
    std::string Det1;
    /** Second detector met by the beam **/
    std::string Det2;
    /** Third detector met by the beam **/
    std::string Det3;
    /** Fourth detector met by the beam **/
    std::string Det4;
    /** Fifth detector met by the beam **/
    std::string Det5;
    /** Sixth detector met by the beam **/
    std::string Det6;
    /** GOLIATH magnet status **/
};

class XMLSettingsReader {

 public :
      XMLSettingsReader( void );
      virtual ~XMLSettingsReader( void );

      int getBaseNodeCount(std::string _nodeName);
      int getNodeChildCount(std::string _nodeName, int _nodeNumber, std::string _childName);

      std::string getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode);

      void getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode, std::string &_retVal);
      void getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode, bool& _retVal);
      void getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode, double& _retVal);
      void getChildValue(std::string _baseNode, int _baseNumber, std::string _childNode, int& _retVal);

      bool parseFile(std::string _fileName);

 private :
      XercesDOMParser* m_DOMParser;
      DOMElement* m_rootNode;
};

#endif
