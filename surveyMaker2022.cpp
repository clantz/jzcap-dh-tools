//The survey coordinate system has x down the beamline axis
void RotateToBeamLineCoords(vector<TVector3*> &vIn){
  for(TVector3* vec: vIn){
    vec->SetXYZ(vec->y(),vec->z(),vec->x());
  }
}

//Take the average cross product of all given vectors such that the component in the specified axis is positive
TVector3 GetAverageNormal(vector<TVector3> vIn, TString axis){
  axis.ToLower();
  int nPoints = vIn.size();
  TVector3 normal(0,0,0);
  TVector3 buffer;
  for(int i = 0; i < nPoints; i++){
    for(int j = i+1; j < nPoints; j++){
      buffer = vIn[i].Cross(vIn[j]);
        if( (axis == "x" && buffer.x() < 0.0) ||
            (axis == "y" && buffer.y() < 0.0) ||
            (axis == "z" && buffer.z() < 0.0) ) buffer = vIn[j].Cross(vIn[i]);
      normal += buffer;
    }
  }
  return normal.Unit();
}


//Parse the survey points to get the position of the center of the active volume for each ZDC on a given side
//as setup in survey 2 of the 2022 test beam
void DoZDCSurvey2(vector<TVector3*> vIn, vector<TVector3> &pos, TQuaternion &dir, TString side){
  //Establish dimensions of the ZDC
  float ZDC_HEIGHT = 0.621; //m
  float ZDC_BOTTOM_TO_CENTER_ACTIVE = 0.09121; //m
  float ZDC_HALF_WIDTH =  0.046; //m
  float ZDC_HALF_DEPTH =  0.077; //m
  float GAP = 0.0005; //m

  side.ToLower(); //just in case I get crazy with the CAPS

  RotateToBeamLineCoords(vIn);

  //Construct all the direction vectors in the survey plane
  vector< TVector3 > vPlane;
  for(int i = 1; i < 3; i++){
    for(int j = i+1; j <= 3; j++){
      vPlane.push_back( TVector3( *vIn[j] - *vIn[i] ) );
    }
  }

  TVector3 vDir = GetAverageNormal(vPlane,"x");//The normal of the outter face of the left ZDCs
  TVector3 backEdge = (*vIn[1] - *vIn[0]).Unit();//Rotate around the vector defined by the edge of the back ZDC
  vDir.Rotate(TMath::Pi()/2, backEdge); //This should be equivalent to the bottom edges of the ZDC. This is now the normal vector of the front face of the ZDC
  float angle = atan( sqrt( 1 - pow(backEdge.y(),2.0) )/backEdge.y() ); //This is approximate
  dir.SetAxisQAngle(vDir,angle);

  TVector3 backPos = *vIn[0] + *vIn[1]; //Average position of the back edge (DT - DB)/2
  backPos *= 0.5;
  backPos.SetY( vIn[3]->y() - ZDC_HEIGHT ); //Survey point on the top of the rear module (TD) - module height

  //Offset from the corner to the center of the rear module
  TVector3 offset( ZDC_HALF_WIDTH, ZDC_BOTTOM_TO_CENTER_ACTIVE, -ZDC_HALF_DEPTH );
  if(side == "left") offset.SetX(-offset.x()); //The survey points were taken on outsides of the setup
  backPos += offset; //Moves from the bottom outside corner of the ZDC to the center of the active volume


  pos.resize(3);
  for(int i = 0; i < 3; i++)
    pos.at(i) = backPos + vDir*i*(2.0*ZDC_HALF_DEPTH + GAP);

}


//Parse the survey poINTS to get the position of the center of the active volume for an RPD
//as setup in survey 2 of the 2022 test beam
void DoRPD(vector<TVector3*> vIn, TVector3 &pos, TQuaternion &dir ){
  float RPD_BOTTOM_TO_CENTER_ACTIVE = 0.081; //m
  float RPD_HALF_DEPTH =  0.015; //m

  RotateToBeamLineCoords(vIn);

  //Position between top two survey points
  TVector3 topCenter = *vIn[2] + *vIn[4];
  topCenter *= 0.5;
  //Position between top two survey points
  TVector3 bottomCenter = *vIn[1] + *vIn[3];
  bottomCenter *= 0.5;
  TVector3 centerLine = (topCenter - bottomCenter).Unit();  //Vertical center line of the front face

  //Construct all the direction vectors in the survey plane
  vector< TVector3 > vPlane;
  for(int i = 1; i < 4; i++){
    for(int j = i+1; j <= 4; j++){
      vPlane.push_back( TVector3( *vIn[j] - *vIn[i] ) );
    }
  }

  TVector3 vDir = GetAverageNormal(vPlane,"z");//The normal of the front face of the RPD (facing downstream)
  vDir.Rotate(TMath::Pi(),centerLine); //Rotate around the center line so the direction vector faces upstream
  float angle = atan( sqrt( 1 - pow(centerLine.y(),2.0) )/centerLine.y() ); //This is approximate
  dir.SetAxisQAngle(vDir,angle); //Convert direction vector to a quaternion

  pos = bottomCenter - centerLine*(bottomCenter.y() - vIn[0]->y() - RPD_BOTTOM_TO_CENTER_ACTIVE); //Go down the center line to just in front of the center of the active area
  pos += -(vDir)*RPD_HALF_DEPTH; //Go from the front of the active area to the center
}


//Add a single XML entry
void addXMLentry(ofstream &file, TString name, int start, int end, TVector3 tablePos, TVector3 pos, TQuaternion dir){
  file <<      "    <Survey>"                                            << endl;
  file << Form("      <start_run>%d</start_run>", start               )  << endl;
  file << Form("      <end_run>%d</end_run>",     end                 )  << endl;
  file << Form("      <detector>%s</detector>",   name.Data()         )  << endl;
  file << Form("      <x_pos>%f</x_pos>",         pos.x()*1000        )  << endl;
  file << Form("      <y_pos>%f</y_pos>",         pos.y()*1000        )  << endl;
  file << Form("      <z_pos>%f</z_pos>",         pos.z()*1000        )  << endl;
  file << Form("      <i>%f</i>",                 dir.fVectorPart.x() )  << endl;
  file << Form("      <j>%f</j>",                 dir.fVectorPart.y() )  << endl;
  file << Form("      <k>%f</k>",                 dir.fVectorPart.z() )  << endl;
  file << Form("      <w>%f</w>",                 dir.fRealPart       )  << endl;
  file << Form("      <table_x>%f</table_x>",     tablePos.x()*1000   )  << endl;
  file << Form("      <table_y>%f</table_y>",     tablePos.y()*1000   )  << endl;
  file <<      "    </Survey>"                                           << endl;
}

void doSurvey1(ofstream &file){

  int startRun = 69741;
  int endRun   = 70068;

  TVector3 tablePos(-0.28177, 0.21595, 0);

  // Establish dimensions of the ZDC
  float ZDC_HEIGHT = 0.621; //m
  float ZDC_BOTTOM_TO_CENTER_ACTIVE = 0.09121; //m
  float ZDC_HALF_WIDTH =  0.046; //m
  float ZDC_HALF_DEPTH =  0.077; //m
  float GAP = 0.0005; //m

  // MEASURED POINTS ON ZDC
  vector<TVector3*> ZDCs;
  ZDCs.push_back( new TVector3( 17.4346,  0.0427, -0.0921) ); //ZDC_LEFT_ULB
  ZDCs.push_back( new TVector3( 17.4344,  0.0463,  0.5358) ); //ZDC_LEFT_ULT
  ZDCs.push_back( new TVector3( 17.4348, -0.0502, -0.0916) ); //ZDC_LEFT_URB
  ZDCs.push_back( new TVector3( 17.4347, -0.0465,  0.5360) ); //ZDC_LEFT_URT
  ZDCs.push_back( new TVector3( 17.4316, -0.5135, -0.0900) ); //ZDC_RIGHT_ULB
  ZDCs.push_back( new TVector3( 17.4326, -0.5131,  0.5370) ); //ZDC_RIGHT_ULT
  ZDCs.push_back( new TVector3( 17.4315, -0.6117, -0.0898) ); //ZDC_RIGHT_URB
  ZDCs.push_back( new TVector3( 17.4323, -0.6132,  0.5365) ); //ZDC_RIGHT_URT

  RotateToBeamLineCoords(ZDCs);

  //Construct all the direction vectors in the survey plane
  vector< TVector3 > vPlane;
  for(int i = 1; i < 7; i++){
    for(int j = i+1; j <= 7; j++){
      vPlane.push_back( TVector3( *ZDCs[j] - *ZDCs[i] ) );
    }
  }

  //These should be the basis vectors for the ZDCs coordinates
  TVector3 ZDC_FACE_NORMAL = GetAverageNormal(vPlane,"x");
  TVector3 ZDC_HORIZONTAL = (*ZDCs[0] - *ZDCs[6]).Unit(); //Arrange the ZDCs in a line formed by the outter bottom survey points
  TVector3 ZDC_VERTICAL = (*ZDCs[0] - *ZDCs[1] + *ZDCs[7] - *ZDCs[6]).Unit();

  TVector3 LEFT_ZDC_CENTER = *ZDCs[0]; //Get the center of the active volume from the corner of the left ZDC
  // TVector3 LEFT_ZDC_CENTER = *ZDCs[0] + ZDC_VERTICAL*ZDC_BOTTOM_TO_CENTER_ACTIVE + ZDC_FACE_NORMAL*ZDC_HALF_DEPTH; //Get the center of the active volume from the corner of the left ZDC
  vector<TVector3> ZDC_POS(7); //There were 7 zdcs in this configuration
  for(int i=0; i<7; i++)
    ZDC_POS[i] = LEFT_ZDC_CENTER + ZDC_HORIZONTAL*i*2*ZDC_HALF_WIDTH;


  float angle = atan( sqrt( 1 - pow(ZDC_VERTICAL.y(),2.0) )/ZDC_VERTICAL.y() ); //This is approximate
  TQuaternion ZDC_DIR(ZDC_FACE_NORMAL, angle);

  //Detector names in the same order as the ZDC_POS vector
  vector<TString> ZDC_NAMES = {"1-2 HAD1", "1-2 HAD2", "1-2 HAD3", "8-1 HAD3", "8-1 HAD2", "8-1 EM", "8-1 HAD1"};

  for(int det=0; det<7; det++)
    addXMLentry(file, ZDC_NAMES[det], startRun, endRun, tablePos, ZDC_POS[det], ZDC_DIR);

}


//Survey2
void doSurvey2(ofstream &file){

  TVector3 tablePos(-0.2093, 0.21589, 0);
  //
  // // MEASURED POINTS ON EM
  // TVector3 EM_LDB(18.2657, 0.0525, 0.3179);
  // TVector3 EM_LDT(18.2660, 0.0545, 0.4495);
  // TVector3 EM_LTD(18.2311, 0.0557, 0.4742);
  // TVector3 EM_LTU(18.1442, 0.0561, 0.4745);
  // TVector3 EM_LUB(18.1042, 0.0537, 0.3422);
  // TVector3 EM_LUT(18.1048, 0.0555, 0.4506);
  int startRun = 70138;
  int endRun   = 70594;

  ///////////////////////////////////////////////////
  //        ARM 1-2
  ///////////////////////////////////////////////////

  // MEASURED POINTS ON Left ZDCs
  vector<TVector3*> ZDC_LEFT;
  ZDC_LEFT.push_back( new TVector3 (18.8941, 0.0412, 0.0318) ); // ZDC_L_LDB
  ZDC_LEFT.push_back( new TVector3 (18.8931, 0.0458, 0.4917) ); // ZDC_L_LDT
  ZDC_LEFT.push_back( new TVector3 (18.8611, 0.0467, 0.5302) ); // ZDC_L_LTD
  ZDC_LEFT.push_back( new TVector3 (18.4114, 0.0504, 0.5367) ); // ZDC_L_LTU

  vector<TVector3> ZDC_LEFT_POS;
  TQuaternion ZDC_LEFT_DIR;
  DoZDCSurvey2(ZDC_LEFT, ZDC_LEFT_POS, ZDC_LEFT_DIR,"left");
  vector<TString> ZDC_LEFT_NAMES = {"1-2 HAD2", "1-2 HAD1", "1-2 EM"};

  //Write them front to back
  for(int det=2; det>=0; det--)
    addXMLentry(file, ZDC_LEFT_NAMES[det], startRun, endRun, tablePos, ZDC_LEFT_POS[det], ZDC_LEFT_DIR);

  // MEASURED POINTS ON RPD
  vector<TVector3*> RPD_LEFT;
  RPD_LEFT.push_back( new TVector3 (18.3382,  0.0408, -0.0834) ); //RPD_L_H
  RPD_LEFT.push_back( new TVector3 (18.3477,  0.0358,  0.1529) ); //RPD_L_LUB
  RPD_LEFT.push_back( new TVector3 (18.3468,  0.0398,  0.3227) ); //RPD_L_LUT
  RPD_LEFT.push_back( new TVector3 (18.3473, -0.0358,  0.1553) ); //RPD_L_RUB
  RPD_LEFT.push_back( new TVector3 (18.3471, -0.0323,  0.3074) ); //RPD_L_RUT

  TVector3 RPD_LEFT_POS;
  TQuaternion RPD_LEFT_DIR;
  DoRPD(RPD_LEFT, RPD_LEFT_POS, RPD_LEFT_DIR);

  addXMLentry(file, "1-2 RPD", startRun, endRun, tablePos, RPD_LEFT_POS, RPD_LEFT_DIR);

  //Place the lead block about 80mm in front of the respective RPD 1/2 block height above the bottom of the RPD foot (RPD_R_H.y() - 10mm)
  //Also give it the same orientation as the RPD
  TVector3 LEAD_LEFT_POS(RPD_LEFT_POS.x(), RPD_LEFT[0]->y() - 0.01 + 0.125, RPD_LEFT_POS.z() - 0.08);
  addXMLentry(file, "1-2 LEAD", startRun, endRun, tablePos, LEAD_LEFT_POS, RPD_LEFT_DIR);

  ///////////////////////////////////////////////////
  //        ARM 8-1
  ///////////////////////////////////////////////////

  // MEASURED POINTS ON Right ZDCs
  vector<TVector3*> ZDC_RIGHT;
  ZDC_RIGHT.push_back( new TVector3 (18.8907, -0.4686, 0.0263) ); //ZDC_R_RDB
  ZDC_RIGHT.push_back( new TVector3 (18.8918, -0.4697, 0.4871) ); //ZDC_R_RDT
  ZDC_RIGHT.push_back( new TVector3 (18.8813, -0.4703, 0.5319) ); //ZDC_R_RTD
  ZDC_RIGHT.push_back( new TVector3 (18.4133, -0.4679, 0.5386) ); //ZDC_R_RTU

  vector<TVector3> ZDC_RIGHT_POS;
  TQuaternion ZDC_RIGHT_DIR;
  DoZDCSurvey2(ZDC_RIGHT, ZDC_RIGHT_POS, ZDC_RIGHT_DIR,"right");
  vector<TString> ZDC_RIGHT_NAMES = {"8-1 HAD2", "8-1 HAD1", "8-1 EM"};

  //Write them front to back
  for(int det=2; det>=0; det--)
    addXMLentry(file, ZDC_RIGHT_NAMES[det], startRun, endRun, tablePos, ZDC_RIGHT_POS[det], ZDC_RIGHT_DIR);

  vector<TVector3*> RPD_RIGHT;
  RPD_RIGHT.push_back( new TVector3 (18.3349, -0.3030, -0.0795) );  //RPD_R_H
  RPD_RIGHT.push_back( new TVector3 (18.3434, -0.3849,  0.1546) );  //RPD_R_LUB
  RPD_RIGHT.push_back( new TVector3 (18.3429, -0.3859,  0.2923) );  //RPD_R_LUT
  RPD_RIGHT.push_back( new TVector3 (18.3432, -0.4555,  0.1593) );  //RPD_R_RUB
  RPD_RIGHT.push_back( new TVector3 (18.3430, -0.4568,  0.3328) );  //RPD_R_RUT

  TVector3 RPD_RIGHT_POS;
  TQuaternion RPD_RIGHT_DIR;
  DoRPD(RPD_RIGHT, RPD_RIGHT_POS, RPD_RIGHT_DIR);

  addXMLentry(file, "8-1 RPD", startRun, endRun, tablePos, RPD_RIGHT_POS, RPD_RIGHT_DIR);

  TVector3 LEAD_RIGHT_POS(RPD_RIGHT_POS.x(), RPD_RIGHT[0]->y() - 0.01 + 0.125, RPD_RIGHT_POS.z() - 0.08);
  addXMLentry(file, "8-1 LEAD", startRun, endRun, tablePos, LEAD_RIGHT_POS, RPD_RIGHT_DIR);

}


void surveyMaker2022(){


  ofstream file("Survey_2022.xml");

  file << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>" << endl;
  file << "<root>" << endl;

  TVector3 TRIGGER_CENTRE(16.5325, -0.0001, -0.0003); //TRIGGER_CENTRE
  TRIGGER_CENTRE.SetXYZ( TRIGGER_CENTRE.y(), TRIGGER_CENTRE.z(), TRIGGER_CENTRE.x() );
  addXMLentry(file, "TRIGGER", 69741, 70594, TVector3(-0.2093, 0.21589, 0), TRIGGER_CENTRE, TQuaternion(0,0,0,0) ); //The table position and orientation aren't important here

  doSurvey1(file);
  doSurvey2(file);

  file << "</root>" << endl;
}
